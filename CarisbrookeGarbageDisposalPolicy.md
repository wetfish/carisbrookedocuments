% GARBAGE DISPOSAL POLICY
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

Residents are asked to take due care with garbage disposal in order to
prevent breeding and attracting pests such as rats, flies and
cockroaches as well as the opportunistic ibis.

Please follow these steps to ensure that we have a vermin free
environment in the building:

-   All garbage must be wrapped securely to prevent rat, fly and
    cockroach access. It must be placed in the red-lidded bins. The lid
    of the bin must always be closed securely. Never leave garbage
    beside the bins or on the footpath. If the bins are not there they
    will be outside in Earl Lane.

-   Waxed cardboard, polystyrene, Styrofoam, drinking glasses, light
    globes or window glass should be wrapped and placed in
    red-lidded bins.

-   Place plastic bags in the red-lidded bins.

-   Recycling items for disposal, such as paper and plastic containers
    with a recycling number 1-7 in the triangle must be rinsed and put
    in the yellow-lidded bins. The lid must always be securely closed.

-   Cardboard boxes should be flattened and then placed in the
    yellow-lidded bins.

-   We strongly recommend that you return your coat hangers to the
    drycleaners or laundry after use. They will appreciate your business
    and this supports recycling.

-   If you are using the rooftop please remove all bottles, food items
    and other garbage. These items should be placed in the appropriate
    red or yellow- lidded bins on the basement floor. Do not leave garbage
    in the bin on the roof.

-   If you have items to dispose of that will not fit in these bins,
    such as mattresses, furniture or old appliances please contact the
    City Council to arrange a pick up. Book online:
    <http://www.cityofsydney.nsw.gov.au/live/waste-and-recycling/book-a-pick-up>
    or call 9265 9333. They also have an e-waste collection
    service for electronic waste such as computers and televisions.

-   Needles or syringes need to be disposed of safely to prevent needle
    stick injury at home and for workers who remove waste or
    clean facilities. Simply capping a needle is not enough. Syringes
    should be treated as clinical waste, like they would be in
    a hospital. Single needle disposal is available at:
    
    -    **Sydney Community Sharps Bin:** Kings Cross Library Toilets, 52
         Darlinghurst Rd, Potts Point
    -    **Clinic 180:** 180 Victoria St, Potts Point
    -    **Sydney Community Sharps Bin:** Fitzroy Park Public Toilets
         Darlinghurst Rd & Macleay St, Potts Point
    -    **Sydney Community Sharps Bin**, Hughes Street Potts Point
         29 Hughes St, Potts Point
    -    **or call 92659333**.
