% RENOVATION GUIDELINES
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

## Introduction

The following Renovation Guidelines are issued by the Board of Directors
of Carisbrooke Home Units Pty Limited ("**Carisbrooke**" or "**the
Company**") under the Articles of Association of the Company, which
charges the Directors of the Company with the management of the business
of Carisbrooke. They are a practical application of the rights and
obligations of the shareholders (or "**members**") of Carisbrooke noted
under the Articles and shall apply from the date of this document. A
copy will be supplied to all members. Future members are to be provided
with a copy. Members are responsible for ensuring that their tenants
comply with the Renovation Guidelines, although it is assumed that
tenants will generally not be undertaking renovations.

## Notice to and Approval of the Board

Notice is to be given to the Board of Directors ("**Board**") of any
proposed renovations. Approval of the Board is required. Proposals will
be reviewed at Board meetings.

## Notice to Neighbours

All residents are to be given notice by way of a letterbox drop of the
renovation works including general details of the scope of works and
estimated dates of the works.

## Period of Notice to Neighbours

**Two \(2) weeks notice:** for renovation works expected to last for less
than 1 week.

**Four \(4) weeks notice:** for renovation works expected to last for up
to 1 month.

**Two \(2) months notice:** for renovation works expected to last for
more than 1 month.

Failure to provide the necessary notice will entitle the Board to
instruct a delay in commencement of works until the relevant notice
period has elapsed.

## Council Approval

If Council approval is required the shareholder undertaking the
renovations shall prepare all necessary documentation (eg development
application etc). As the legal owner of the entire building the Company
signs the relevant documentation for submission to Council, however the
shareholder or their architects etc shall be responsible for lodging the
forms and following up with Council.

## Structural Changes

If structural changes are proposed, e.g. moving doorways, openings in
walls etc then full details must be given to the Board and the Board may
require a structural report. Currently the Company uses Costin Roe
Consulting Pty Limited as its standard structural engineers.

The cost of the report will be at the expense of the shareholder
requesting approval to the renovations.

## Bond

A bond of $5000 must be paid to the Managing Agent before any building
work commences. The Board may, in its absolute discretion, waive the
requirement for bond following request by a shareholder and where the
risk of damage to common areas is considered sufficiently remote. The
bond will be deposited into the Administration Fund of the Company.
Subject to the cost of any repairs as noted in the "Damages" section
below, the bond will be refunded to the shareholder after completion of
the works following written request to the Managing Agent and the
consent of the Board.

## Damages & Indemnity

Any damage to the common areas of the building caused by the
renovations, will be repaired at the expense of the shareholder. The
Board will determine in its absolute discretion (such discretion to be
reasonably exercised) when damage has been caused by renovations, and
when the repairs to the common areas are to be made.

The Company will retain the renovation bond until all repairs have been
made to the satisfaction of the Board. Any cost of repairs in excess of
the bond amount will be levied on the shareholder, and by undertaking
the renovation work the shareholder agrees to indemnify the Company for
any damage to common areas caused as a result of the renovation work.

## Hours of Work

Monday to Saturday - 8 am to 4 pm (except Easter and Christmas
holidays). These hours are to be strictly adhered to and should any work
be conducted outside of these hours any Director may instruct the
workers to cease work.

## Parking

No parking is available at Carisbrooke.

## Lifts

Lift curtains must be installed when bringing in or taking out building
materials. Lift curtains are stored in the utility room on the roof. The floor
of the lift is to be covered with plastic sheeting.

## Floor Covers

The foyer of the floor providing access to the unit being renovated as
well as the lower ground level floor is to be covered with plastic
sheeting which it to be tape sealed approximately 15cm up the tile
walls. All tape marks are to be removed after completion of the works.

## Wall Covers

No items of rubbish, building materials or furniture are to be rested
against the walls of the building, in particular the wood paneling in
the main foyer of the building. The only exception is if due to size and
lightweight (eg wallboard) it is necessary to rest it against the wall
to prevent obstruction of the common areas. However, if any items are
laid against the walls/wooden paneling with or without padding any
damage subsequently found will prima facie be attributed to the
renovation works and its repair will be for the cost of the shareholder
undertaking the renovations.

## Trade Entrance and Rubbish Removal

The rear entrance (Earl Street) and lower ground foyer is to be used as
the trade entrance, not the main foyer or front entrance. All rubbish
and building materials are to be brought in an out through the rear
entrance of the building and the lower ground level. All rubbish skips
are to be kept at the rear of the building (Earl Street.)

**THE GROUND FLOOR MAIN FOYER IS NOT TO BE USED BY TRADESPEOPLE AND NO
BUILDING MATERIALS ARE TO ENTER OR LEAVE THE BUILDING THROUGH THE MAIN
FOYER.**

The only exception to this rule is if large items of furniture cannot be
brought into the building via the rear trades entrance due to size
restrictions.

## Containing Works

During loud works and any demolition, all windows and doors to the
apartment under renovation are to be kept shut and an effort to be made
to seal the gaps around the entry door to that apartment.

## Plumbing

Turning off the water to the building automatically shuts down the communal hot
water service. Any shareholder or their builder/plumber who turns off the water
to the building does so at their own risk as any costs incurred to get the hot
water restored will be passed onto that shareholder.

## Preferred Trades people

Given the relationship held with the building's preferred trades people,
it is highly recommended that these trades people be used when plumbing or
electrical work is required.

### Preferred Electrician

Patrick Chapman

Ph. 0451 660 911

### Preferred Plumber

Mr Washer Plumber & Electrician to the Rescue

Ph. 1300 679 274

Email: jobs@mrwasher.com.au


## Supervision of Workers

Shareholders must give a copy of the Renovation Guidelines to workers
and must supervise the bringing into or out of materials. This is to be
strictly observed if materials are required to be brought in via the
front entrance (Springfield Ave) and if the lift is to be used.

If the shareholder will not be personally supervising the workers they
must appoint a supervisor and the name and contact telephone number of
the supervisor will be given to the Managing Agent and/or Board. The
supervisor is to meet with the Managing Agent, or the Board or a
Director prior to commencement of the work.

## List of Tradespersons

A list of all tradespersons and their contact details is to be provided
to the Managing Agent.

**_Banning of Tradespersons_**

If tradespersons are considered by the Board (in its absolute
discretion) to have engaged in bad practice during renovation work then
the tradespersons will be banned from further work in the building.

\hfill\break

---

\clearpage

## Quick Checklist for Renovations

1.  Have I notified the Board?
    
2.  Is Council approval required? - Are structural works involved?
    
3.  Have I done a letterbox drop to residents?
    
4.  Have I paid the $5000 bond?
    
5.  Is plumbing or electrical work required - do I need to involve
    Mr Washer or Patrick Chapman?
    
6.  Have I given a copy of the renovation guidelines to the
    tradespersons / workers and told them that I will be responsible for
    any damage to common areas that they cause?
    
7.  Will I be supervising them or do I have to appoint a supervisor?
    Have I given the supervisor's name and telephone number to the
    Managing Agent or the Board? Has the appointed supervisor met with
    the Managing Agent / Board / Director?
    
8.  Are the workers aware that a Director can instruct them to stop work
    if outside the hours 8 am - 4 pm Monday to Saturday?
    
9.  Have I protected the lift and floor of my foyer and the lower
    ground?
    
10. Have I given a list of tradespersons / workers to the Managing Agent
    / Board?
