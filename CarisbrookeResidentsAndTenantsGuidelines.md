% RESIDENTS & TENANTS GUIDELINES
---
date: November 2020
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

\newpage
# House Rules

{{tmp/CarisbrookeHouseRules.md}}

\newpage
# Rooftop Protocol

{{tmp/CarisbrookeRooftopProtocol.md}}

\newpage
# Leasing Policy

{{tmp/CarisbrookeLeasingPolicy.md}}

\newpage
# Moving Guidelines

{{tmp/CarisbrookeMovingGuidelines.md}}

\newpage
# Garbage Disposal Policy

{{tmp/CarisbrookeGarbageDisposalPolicy.md}}

\newpage
# Keeping of Animals Policy

{{tmp/CarisbrookeKeepingOfAnimalsPolicy.md}}

\newpage
# Bicycle Policy

{{tmp/CarisbrookeBicyclePolicy.md}}

\newpage
# Renovation Guidelines

{{tmp/CarisbrookeRenovationGuidelines.md}}

\newpage
# Tradespeople and Contacts

{{tmp/CarisbrookeTradespeopleAndContacts.md}}
