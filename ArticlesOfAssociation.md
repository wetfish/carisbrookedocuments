% Corporations Act 2001 (Cth) \
  A Company Limited by Shares. \
  \
  Articles of Association \
  of \
  CARISBROOKE HOME UNITS PTY. LIMITED
  
---
date:  July 2018
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \usepackage{xcolor}
- \usepackage{sectsty}
- \usepackage{tocloft}
- \sectionfont{\centering}
---

\newpage

---

# Articles of Association of \
CARISBROOKE HOME UNITS PTY. LIMITED

{{Articles/Article_001.md}}

{{Articles/Article_002.md}}

## Shares.

{{Articles/Article_003.md}}

{{Articles/Article_004.md}}

{{Articles/Article_005.md}}

{{Articles/Article_006.md}}

{{Articles/Article_007.md}}

## Share Certificate.

{{Articles/Article_008.md}}

{{Articles/Article_009.md}}

## Calls.

{{Articles/Article_010.md}}

{{Articles/Article_011.md}}

{{Articles/Article_012.md}}

{{Articles/Article_013.md}}

{{Articles/Article_014.md}}

{{Articles/Article_015.md}}

## Forfeiture of Shares.

{{Articles/Article_016.md}}

{{Articles/Article_017.md}}

{{Articles/Article_018.md}}

{{Articles/Article_019.md}}

{{Articles/Article_020.md}}

{{Articles/Article_021.md}}

{{Articles/Article_022.md}}

## Lien on Shares.

{{Articles/Article_023.md}}

{{Articles/Article_024.md}}

{{Articles/Article_025.md}}

## Transfer and Transmission of Shares.

{{Articles/Article_026.md}}

{{Articles/Article_027.md}}

{{Articles/Article_028.md}}

{{Articles/Article_029.md}}

{{Articles/Article_030.md}}

## Restricted Right of Transfer.

{{Articles/Article_031.md}}

{{Articles/Article_032.md}}

## Increase Reduction and Alteration of Capital.

{{Articles/Article_033.md}}

{{Articles/Article_034.md}}

## General Meetings.

{{Articles/Article_035.md}}

{{Articles/Article_036.md}}

{{Articles/Article_037.md}}

## Proceedings at General Meetings.

{{Articles/Article_038.md}}

{{Articles/Article_039.md}}

{{Articles/Article_040.md}}

{{Articles/Article_041.md}}

{{Articles/Article_042.md}}

{{Articles/Article_043.md}}

{{Articles/Article_044.md}}

## Voting at General Meetings.

{{Articles/Article_045.md}}

{{Articles/Article_046.md}}

{{Articles/Article_047.md}}

{{Articles/Article_048.md}}

{{Articles/Article_049.md}}

{{Articles/Article_050.md}}

{{Articles/Article_051.md}}

{{Articles/Article_052.md}}

## Directors.

{{Articles/Article_053.md}}

{{Articles/Article_054.md}}

{{Articles/Article_055.md}}

{{Articles/Article_056.md}}

{{Articles/Article_057.md}}

{{Articles/Article_058.md}}

{{Articles/Article_059.md}}

{{Articles/Article_060.md}}

{{Articles/Article_061.md}}

{{Articles/Article_062.md}}

{{Articles/Article_063.md}}

{{Articles/Article_064.md}}

## Meetings of Directors and Committees.

{{Articles/Article_065.md}}

{{Articles/Article_066.md}}

{{Articles/Article_067.md}}

{{Articles/Article_068.md}}

{{Articles/Article_069.md}}

{{Articles/Article_070.md}}

{{Articles/Article_071.md}}

{{Articles/Article_072.md}}

{{Articles/Article_073.md}}

{{Articles/Article_074.md}}

{{Articles/Article_075.md}}

## Powers of Directors.

{{Articles/Article_076.md}}

{{Articles/Article_077.md}}

## Appointment by Director of a Proxy.

{{Articles/Article_078.md}}

## Attorney of Member.

{{Articles/Article_079.md}}

## Minutes.

{{Articles/Article_080.md}}

## Dividends and Reserve Funds.

{{Articles/Article_081.md}}

{{Articles/Article_082.md}}

{{Articles/Article_083.md}}

{{Articles/Article_084.md}}

{{Articles/Article_085.md}}

{{Articles/Article_086.md}}

{{Articles/Article_087.md}}

{{Articles/Article_088.md}}

{{Articles/Article_089.md}}

{{Articles/Article_090.md}}

{{Articles/Article_091.md}}

{{Articles/Article_092.md}}

{{Articles/Article_093.md}}

{{Articles/Article_094.md}}

{{Articles/Article_095.md}}

## Accounts.

{{Articles/Article_096.md}}

{{Articles/Article_097.md}}

{{Articles/Article_098.md}}

## Audit.

{{Articles/Article_099.md}}

## Notices.

{{Articles/Article_100.md}}

{{Articles/Article_101.md}}

{{Articles/Article_102.md}}

{{Articles/Article_103.md}}

{{Articles/Article_104.md}}

## Signing of Cheques Bills Contracts Etc.

{{Articles/Article_105.md}}

{{Articles/Article_106.md}}

## The Seal.

{{Articles/Article_107.md}}

## Indemnity.

{{Articles/Article_108.md}}

## Winding Up.

{{Articles/Article_109.md}}

{{Articles/Article_110.md}}



WE, the Directors whose names are subscribed below hereby agree to
the foregoing Articles of Association.

------------------------------------------------------------------------
Name                    Division  Units Represented Contact Details    
----------------------- --------- ----------------- -------------------
Andrew Smith            One       1 - 4 & 1A        Ph. 0418 210 759

David Waite             Two       5 - 7             Ph. 0414 385 162

Paul McKeon             Three     8 - 10            Ph. 0414 596 266

Susan Wheeldon          Four      11 - 14           Ph. 0431 747 145

Tara Czinner            Five      15 - 17           Ph. 0401 808 038

Brad Buckley            Six       18 - 20           Ph. 0439 524 222

Bronwyn Davies (Chair)  Seven     21 - 23           Ph. 0407 212 943

Jason Prior             Eight     24 - 25           Ph. 0408 463 202

Jock Pharey             Nine      26 - 27           Ph. 0419 703 814

Greg Bayles             Ten       28                Ph. 0407 001 947
------------------------------------------------------------------------

DATED this tenth day of July 2018
