% HOUSE RULES
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

## Introduction

The following House Rules for residents are issued by the Board of
Directors of Carisbrooke Home Units Pty Limited ("**Carisbrooke**" or
"**the Company**") under the Articles of Association of the Company,
which charges the Directors of the Company with the management of the
business of Carisbrooke. They are a practical application of the rights
and obligations of the shareholders (or "**members**") of Carisbrooke
noted under the Articles and shall apply from the date of this document.
The Board may in its absolute discretion make exceptions to the rules if
it is in the best interests of the Company.

A copy of the House Rules will be supplied to all residents - members
and tenants. Members are responsible for ensuring that their tenants
comply with the House Rules; and to this extent, the House Rules
applying from time to time must be included as part of the tenancy
agreement between a member and their tenant.

Any enquiries or complaints in relation to the House Rules should be
directed to the Managing Agent of Carisbrooke, currently Alldis & Cox,
telephone \(02) 9326 4488, attention: Stuart Cox or a member of the Board
of Directors can be contacted.

## General

{{HouseRules/General_001.md}}

{{HouseRules/General_002.md}}

{{HouseRules/General_003.md}}

{{HouseRules/General_004.md}}

{{HouseRules/General_005.md}}

{{HouseRules/General_006.md}}

{{HouseRules/General_007.md}}

{{HouseRules/General_008.md}}

{{HouseRules/General_009.md}}

{{HouseRules/General_010.md}}

{{HouseRules/General_011.md}}

{{HouseRules/General_012.md}}

{{HouseRules/General_013.md}}

{{HouseRules/General_014.md}}

{{HouseRules/General_015.md}}

{{HouseRules/General_016.md}}

{{HouseRules/General_017.md}}

{{HouseRules/General_018.md}}

{{HouseRules/General_019.md}}

{{HouseRules/General_020.md}}

{{HouseRules/General_021.md}}

## Repairs and Maintenance

{{HouseRules/RepairsAndMaintenance_001.md}}

## General Safety & Contact Details

{{HouseRules/GeneralSafetyAndContactDetails_001.md}}

## Contents Insurance

{{HouseRules/ContentsInsurance_001.md}}

## Fire Safety

{{HouseRules/FireSafety_001.md}}

{{HouseRules/FireSafety_002.md}}

{{HouseRules/FireSafety_003.md}}

{{HouseRules/FireSafety_004.md}}

{{HouseRules/FireSafety_005.md}}
