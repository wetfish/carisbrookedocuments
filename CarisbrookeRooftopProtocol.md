% ROOFTOP PROTOCOL FOR RESIDENTS AND THEIR GUESTS
---
date: November 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

## General Rooftop Usage Policy
Please observe the following when using the rooftop at any time:

-   Clean up after yourselves.

-   Return all furniture to its correct position.

-   No cigarette butts are to be dropped or placed anywhere on the roof except
    in the ashtrays provided.

-   Put bottles and rubbish into the bins downstairs.

-   Empty and clean ashtrays. Do not remove them from the rooftop.

-   Do not leave butts or food stuff (including grease) on tables, in
    plant pots, or on the floor.

-   Do not leave washing on the line, or clothes in the laundry for more
    than 24 hours.

-   Do not leave clothes soaking in either tub for periods beyond
    24 hours.

-   Never throw anything off the roof.

-   Laundry hours are 7.00 a.m. to 11.00 p.m. daily - machines are not
    to be used outside these hours.

-   11.00 p.m. deadline for any noise on the roof. (We ask that other
    buildings stop all noise at 11.00 p.m.)

-   Rooms on rooftop are not to be used for storage of residents' belongings.

\clearpage

## Roof Party Policy

Resident/s who are holding functions are responsible for the behaviour
and actions of their guests. Residents must ensure their guests do not
interfere with the reasonable peace, comfort or privacy of other
residents

Please be mindful that the rooftop is a communal space and that booking it for
a party does not entitle you to exclusive use of the space, or to require that
people cease using the facilities such as washing machines and clothes lines.
You may ask people to take down their washing, but you may not require or expect
them to do so, nor take down washing on their behalf.

### Roof-Party Rules

1.  The rules apply to any group of **ten \(10) or more** people who are using
    the roof.
    
2.  Roof must be vacated by 11.00pm.
    
3.  The Board will require the resident holding the party to pay a
    refundable deposit of $20.00 per guest prior to holding the party.
    This is to be left with a member of the Board who will sign for
    its receipt.
    
4.  Refund of deposit will depend on:
    
    -   Roof being vacated by 11.00pm;
        
    -   Roof being left clean with all furniture returned to its
        correct position;
        
    -   Guests not interfering with the reasonable peace, comfort or
        privacy of other residents (this shall be determined by the
        Board following the party).
        
    -   The Board retains the right to hire a Security Guard the cost of
        which shall be born by the holder of the party.


### Resident/s Responsibilities

1.  Residents holding a function for between **ten \(10)** and **twenty
    \(20)** guests (inclusive), are required to advise all other residents in
    writing giving at least 7 days notice.
    
2.  Residents holding a function with **more** than **twenty \(20)**
    guests, are required to:
    
    -   Seek approval in writing from the Board of Director's of
        Carisbrooke giving at least **two \(2)** weeks notice
        
    -   Advice is to include the following:
        
    -   Resident holding the party.
        
    -   Date and time of the party.
        
    -   Number of people attending.
        
    -   Measures to be put in place to ensure their guests do not
        interfere with the reasonable peace, comfort or privacy of other
        residents.


### Guests

1.  Residents are reminded that they only should admit their guests to
    our building
    
2.  Residents must ensure their guests leave the roof neat, clean and
    tidy after the function


### Damage to Roof and Other Common Property

Damage to the roof is the responsibility of the resident/s who is/are
holding a function on the roof. The cost to repair or clean shall be
borne by the resident/s who held the function.
