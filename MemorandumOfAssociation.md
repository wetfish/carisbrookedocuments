% Corporations Act 2001 (Cth) \
  A Company Limited by Shares. \
  \
  Memorandum of Association \
  of \
  CARISBROOKE HOME UNITS PTY. LIMITED

---
date:  June 1957
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \usepackage{alphalph}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \sectionfont{\centering}
- \usepackage{etoolbox}
- \patchcmd{\quote}{\rightmargin}{\leftmargin 2em \rightmargin}{}{}
---

\newpage

---

1. The name of the Company is "Carisbrooke Home Units Pty. Limited".

2. The Registered Office of the Company will be situate at Sydney in the State
of New South Wales or elsewhere in New South Wales aforesaid as may from time to
time be determined by the Directors

3. The objects for which the Company is established are:-
    
    a. to exercise all rights and meet all obligations of legal ownership of the
    subject land and building;
    
    b. to provide residential home units for the use of members and to maintain
    the property to a high standard of amenity;
    
    c. to enter into contracts as necessary to build/maintain/improve/alter the
    building;
    
    d. to maintain and enforce regulations for the good management and
    administration of the home unit complex;
    
    e. to lease, licence or otherwise let residential home units on terms the
    Company determines;
    
    f. to guarantee the title to or quiet enjoyment of property of the Company
    to members on the terms the Company determines;
    
    g. to obtain and maintain insurance for the property;
    
    h. to invest monies and the property of the Company and otherwise manage the
    financial resources of the Company for the benefit of members, in accordance
    with the requirements of the law, and on terms the Company deems fit; and
    
    i. to maintain records and accounts of the Company in accordance with
    the requirements of the Corporations Act and for the benefit of members.
    
    AND IT IS HEREBY DECLARED that in the interpretation of this clause the
    meaning of any of the Company's objects shall not be restricted by reference
    to any other object or by the juxtaposition of two or more objects and that
    in the event of any ambiguity this clause shall be construed in such a way
    as to widen and not to restrict the powers of the Company.
    
4. The liability of the members is limited.

5. The capital of the Company is &pound;65,550 divided into 65550 shares of
   &pound;1 each with power for the Company to increase or reduce such capital
   and to issue any part of its capital, original or increased with or without
   any preference priority or special privilege or subject to any postponement
   of rights or to any conditions or restrictions; and so that unless the
   conditions of issue shall otherwise expressly declare every issue of shares
   whether declared to be preference or otherwise shall be subject to the power
   hereinbefore contained.

WE, the several persons whose names and addresses and descriptions are
subscribed are desirous of being formed into a Company in pursuance of this
Memorandum of Association and we respectively agree to take the number of shares
in the capital of the Company set opposite our respective names.

-----------------------------------------------------------------------------------------
  Names, Addresses and Descriptions  No. of Shares taken      Witness to Signatures
            of Subscribers           by each Subscriber
------------------------------------ -------------------- -------------------------------
Harry Brunen                                 one          Alan Blum Solicitor, Sydney.
2 Ithaca Road,
Elizabeth Bay,
Sydney
  
Company Director


Mendel Brunen                                one          Alan Blum
2 Ithaca Road,
Elizabeth Bay
Sydney
  
Company Director
-----------------------------------------------------------------------------------------

DATED this nineteenth day of June 1957
