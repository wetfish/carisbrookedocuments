% GUIDELINES FOR MOVING IN AND OUT OF THE BUILDING
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

When moving please observe the following guidelines which have been
drawn up in the interests of reducing damage (which has associated
costs) to our home, and thinking of our fellow residents and neighbours.

-   Before any furniture or heavy articles are moved into or out of the
    building, written notice must be given to the Company and the prior
    written approval of the Directors obtained. Any damage caused by
    such moving shall be made good at the expense of
    the Occupier who causes it and, in the case of an Occupier who is a
    tenant, the relevant Member is primarily responsible for making good
    damage caused by the tenant. All posts, stair rails, entrances to
    lifts or places where damage is likely to occur shall be adequately
    protected.

-   A security deposit of $350.00 must be paid by the member before
    approval will be given. The deposit is to be paid at least
    24 hours before moving in or out. The Regulations provided for the
    deposit to be applied to pay for any damage.

-   Deposits are to be made by cash or bank cheque to the Managing Agent
    at least 24 hours before moving. The deposit will be deposited in
    The Administration Fund of the Company. The deposit will be retained
    until, in the case of a tenant, the tenant has finally moved out,
    any damage paid for and other costs met, or, in other cases, any
    damage is paid for and other costs met. The deposit will be applied
    to pay for:
    
      i.  any damage or other loss caused as result of the moving; and
      
      ii. any fees or other costs incurred by the Company in providing
          caretaker or other supervisory services to assist or monitor the
          moving.
    
    After deduction of any of those amounts, the balance of the deposit
    (if any) will be refunded following written request to the Managing
    Agent and the approval of the Directors. Any interest earned will,
    after deduction of any bank fees or other expenses, and any taxation
    liability incurred thereon, be paid to the tenant.

-   The Directors may determine that a non-refundable fee for supervision
    of any move is to be paid. If so, the amount of such fee shall be
    notified in writing to the member by the Managing Agent and the
    member must pay such fee by cash or bank cheque to the Managing
    Agent at least 24 hours before the move. Any approval to move in or
    out is conditional on payment of such fee.

-   Moving in and out is only to occur on Mondays to Saturdays between
    7.30am and 5pm or, with approval, Sunday between 11 am and 4 pm, and
    only after prior notice is given to OGL via our managing agents
    Alldis & Cox (Coogee) Pty Ltd, Randwick. PH: 9326 4488, FAX 9326
    4499, EMAIL: [christopher@alldiscox.com.au]
    (mailto:Christopher@alldiscox.com.au)

-   Removalists are to take care with trolleys on the front stairs and
    must use pneumatic tyred trolleys to avoid damaging
    and marking the stairs.

-   The cost of repairing any damage to the building or common areas,
    and for any additional cleaning required, will be charged to the
    owner of the home unit connected to the move. It is the owner's
    choice and responsibility to pass the cost to a tenant
    if applicable.

-   Any household items not required are NOT to be dumped in our
    garbage area. The resident is responsible for arranging for their
    removal by contacting **Sydney Council Waste Services Department**.
    
    - http://www.cityofsydney.nsw.gov.au/live/waste-and-recycling/book-a-pick-up
    - 02 9265 9333

    This is a free service on a Wednesday and to be booked on the prior
    Monday or Tuesday. The resident is responsible for taking the items
    down to Earl Lane on the appropriate Wednesday. This service is for
    metal household appliances (small and large), furniture, soft
    furnishings, carpet, toys, TVs, heaters, pots and pans. You can also
    arrange for disposal of household paints, thinners, cleaners and
    engine oil.

-   Please see the **Garbage Disposal Policy** for more information.

Your consideration in these matters will be appreciated.

\hfill\break

---

\hfill\break

I have read, understood and agree to these conditions: (please return to
Alldis & Cox)

SIGNED: _________________________________

NAME: _________________________________

UNIT NUMBER: _________________________________

DATE: _________________________________

PHONE CONTACT: _________________________________

**ANTICIPATED DATE AND TIME OF MOVE**: _________________________________

DEPOSIT OF $350 ENCLOSED
