3.  **(Company's right to enter)** The Company by its servants and
    agents and employees shall have the right at all reasonable times to
    enter such apartment for all reasonable purposes in connection with
    the inspection of the state of cleanliness, order, and condition;
    and in connection with the inspection, installation, repair,
    maintenance or alteration of gas, water and electrical fittings,
    pipes, wires or other apparatus serving or affecting the service of
    gas, water or electricity to other parts of the building and in
    connection with any other necessary purposes.
