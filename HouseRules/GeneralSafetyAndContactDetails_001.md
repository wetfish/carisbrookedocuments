Residents shall provide the Managing Agent with the name and contact details of
each permanent resident of their apartment, as well as a name and contact
details of a person to contact in case of emergency. The Managing Agent will
keep a register of residents' names and emergency contact names and details.
