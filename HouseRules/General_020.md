20. **(Locks on doors)** No additional locks are to be installed in the
    doors to the apartments. As the entrance door to each apartment is a
    special fire door it is forbidden to install security eyes, door
    bells or additional locks.