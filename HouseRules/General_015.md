15. **(No hanging of laundry)** No articles of laundry, bedding,
    clothing, pot plants or any other items shall be displayed from or
    hung out of any window or balcony without the approval by the Board
    in its absolute discretion. In exercising its discretion the Board
    will have regard to the general character and appearance of the
    building of Carisbrooke.
