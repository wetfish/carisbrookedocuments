1.  Fire hoses are placed between floors on staircase landings. Be sure
    you know their exact location and their functions. If you note any
    damage to hoses please report this to the Managing Agent.
