12. **(No trade exhibits on premises)** No trade, business or
    advertising matter will be exhibited in, on or about an apartment or
    the building of Carisbrooke.
