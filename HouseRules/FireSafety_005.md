5.  To comply with fire regulations:
    
    -   No newspapers, magazines, bottles, cartons, or rubbish of any
        kind are to be left on the landings at any time. Rubbish bins
        are available in the southern alleyway - rear entrance from the
        lower ground. Should a resident be ill or incapacitated then
        they should make private arrangements with the cleaner to pick
        up rubbish from the apartment, not outside on the landing.
        
    -   Cigarette butts are not to be dropped or left anywhere on the roof,
        except in the ashtrays provided. Residents are responsible for ensuring
        that they and their guests dispose of butts in ashtrays or remove them
        from the roof to dispose of them appropriately elsewhere. Dropped
        cigarettes cause a serious fire hazard.
        
    -   No cooking is allowed on the roof top deck or tables.
        
    -   No bicycles, prams, toys or pot plants or other items are to be
        left on any landing or in any passageway at any time.
        Passageways must be kept clear at all times. The Board may
        remove any such item from the passageway at the expense of the
        owner of the item.
        
    -   No flammable items should be stored in the boiler room.
