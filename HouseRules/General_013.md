13. **(No damage)** Residents will not throw or allow to fall any paper,
    rubbish, refuse or other substance out of the windows or doors or
    down the staircases, passages of the building. Any damage or cost
    for cleaning or repair caused by breach hereof shall be borne by the
    resident, and in the case of breach by a tenant, their landlord
    member of the Company.
