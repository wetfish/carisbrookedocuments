11. **(No animals)** No animals shall be kept in or about the building
    unless approved in writing by the Board at its sole discretion.
