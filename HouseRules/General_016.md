16. **(No auctions)** Residents shall not permit any auction or other
    sale of goods to be conducted anywhere in the building or grounds.
