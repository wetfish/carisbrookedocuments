19. **(Roof parties)** Roof parties should be advised to other residents
    at least 1 day in advance and at least 7 days if more than 10 guests
    are attending. Partying on the roof area is not allowed after 11 pm.
    Please refer to related Rooftop Protocol.
