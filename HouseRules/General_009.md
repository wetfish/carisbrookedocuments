9.  **(Noise - flooring)** Residents should ensure that flooring is
    sufficiently covered to prevent the transmission of noise.
