21. **(Plumbing services)** When plumbing work requires shutting down
    water supply, at least 1 day's notice is to be given to all
    residents prior to the shut down.