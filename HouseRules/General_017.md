17. **(Removal of packaging)** Residents are responsible to remove
    packing cases, carton or other large items brought into
    the building. The depositing of such items in any part of the common
    areas of the building is not allowed.
