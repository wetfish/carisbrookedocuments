4.  **(No structural alterations)** Residents shall not, without the
    written consent of the Company, make any structural alternations or
    additions or improvements to the apartment; nor will the member
    allow any additional power points, wiring and light fittings to be
    installed without written permission of the Board.
