2.  **(Common Areas)** Residents shall not damage the common areas of
    the building (including the lift), and any damage or cost for
    cleaning or repair caused by breach hereof shall be borne by the
    members and/or their tenants. Therefore care should be taken when
    bringing in or taking out large items or furniture or other objects.
