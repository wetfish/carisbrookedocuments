10. **(Noise - audio equipment)** Audio equipment is to be used
    responsibly, and in any event must be turned low at 11 pm so as not
    to be unduly heard by other residents.
