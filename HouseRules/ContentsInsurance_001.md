Members and Residents shall procure full contents insurance for their apartment,
and the Company expressly disclaims any liability arising from failure of a
member or resident to take out such insurance.