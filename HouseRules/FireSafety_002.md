2.  A smoke alarm, fire blanket and small extinguisher have been
    provided to each apartment. Please report to the Managing Agent if
    any of these items are missing or not in working order. These items
    are to be replaced at each member's expense.
