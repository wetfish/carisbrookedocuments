1.  **(Apartment Interiors)** Residents shall ensure that at their own
    expense the interior of their apartment and fixtures therein are
    kept clean and in good order and condition.
