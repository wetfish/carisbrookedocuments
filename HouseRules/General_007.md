7.  **(No interference or annoyance)** Residents shall not permit to be
    done in their apartment or in the passageways, lift, entrances,
    grounds or other common areas anything that may tend to interfere
    with or annoy other residents.
