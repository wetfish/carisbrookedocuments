18. **(Laundry)** Laundry facilities on the roof are not to be used
    between the hours of 11 pm and 7 am.
