8.  **(Fire)** Residents shall not permit any activity that will
    increase the rate of fire insurance or conflict with any laws
    relating to fires.
