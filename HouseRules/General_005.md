5.  **(Comply with renovation guidelines)** Residents are to comply with
    any renovation guidelines issued by the Board from time to time.
