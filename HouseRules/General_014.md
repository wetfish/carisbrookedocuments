14. **(Stairways and roof)** Residents and their guests and children are not to
    obstruct stairways, or create a nuisance in the lift or on the roof.
    Residents and their guests are responsible for the safety of their children
    when in any of the common areas of the building.
