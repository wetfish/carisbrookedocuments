% LEASING POLICY
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

>  *Note: In this document, the expression "lease" refers to the leasing of a unit
>  at Carisbrooke by a shareholder in the Company to another person (a
>  "tenant").*


A shareholder in the Company is permitted to lease their unit at
Carisbrooke for a *minimum* twelve month term if that shareholder has
owned the shares entitling them to occupy the unit for 2 years *and*
has been resident at Carisbrooke for two years (a "**Qualifying
Shareholder**") provided that:

1.  The procedure under the heading "Leasing Procedure" below is
    complied with by the Qualifying Shareholder; and
    
2.  If a tenant in occupation of a unit at Carisbrooke breaches the
    House Rules, the Board is entitled to require the Qualifying
    Shareholder to arrange for the tenant to vacate the unit.

Leasing Procedure
-----------------

1.  The Qualifying Shareholder (or their agent) must notify the
    Managing Agent in writing of their intention to lease their unit
    and arrange for the prospective tenant(s) to be interviewed by
    the Board.
    
2.  The shareholder must arrange, with at least 7 days notice, for the
    tenant to be interviewed and for payment of a $100 interview fee
    prior to the interview.
    
3.  At least 48 hours prior to interview the prospective tenant(s) must
    provide at least two references to the Board (or their
    representatives on the interview panel).
    
4.  The prospective tenant must have been given a copy of all Company
    rules, policies and guidelines prior to the interview, and *must be able to
    demonstrate familiarity* with those rules and willingness to comply with
    them.
    
5.  The Board will advise in writing through the Managing Agent its
    decision to approve such prospective tenant(s) as soon as reasonably
    practicable after interviewing such prospective tenant(s).
    
6.  If the Board does not approve a prospective tenant it will advise
    the Qualifying Shareholder in writing in reasonable detail the
    reason why such prospective tenant has not been approved by the
    Board.
    
7.  If the Board approves a prospective tenant the shareholder must pay
    an administration fee (in addition to the interview fee) of $300.
    
8.  An up-to-date copy of the House Rules must be inserted in any lease
    document between tenant(s) and the Qualifying Shareholder and
    the tenant(s) must be obliged to comply with the House Rules within
    the terms of such lease.
    
9.  The name(s) of tenants, their phone number(s), emergency contact
    details and email address(es), and the details of the relevant
    Qualifying Shareholder's agent, must be given to the Board or its
    representatives at interview, and a copy of any lease document
    between the tenant and the Qualifying Shareholder, along with those
    contact details must be provided to the Managing Agent by
    the shareholder.
    
10. If a shareholder leases their unit to a tenant and such
    shareholder is not a Qualifying Shareholder, the Board is entitled
    to require the Qualifying Shareholder to arrange for the tenant to
    vacate the unit.
    
11. A tenant is not permitted to sub-lease a unit, or to extend the
    contract to include additional tenants, without prior approval of
    the Board; no more than 3 tenants will be approved under any one
    contract.
    
12. If a shareholder leases their unit to a tenant, the shareholder
    shall lodge with the Company a $1,000.00 AUD bond that will be
    accessed by the Board in the event that that shareholder's tenant
    causes damage (for example, in the course of a move into or out of
    the building) to the common areas of Carisbrooke that requires
    remediation.
    
    This Bond:
    
    -   is refundable only when the shareholder takes up residence or
        sells their shares.
        
    -   must be replenished by the Shareholder if it has to be accessed
        at any time.
        
    -   does not limit the Board's right and authority to recover
        additional remediation expenses from the shareholder where they
        exceed the amount of the bond lodged.
    
13. The maximum period for which a shareholder may lease their
apartment is 36 months, following which a further qualifying period of
full time residence for 12 months will be required, except where the
tenant is a long term tenant i.e. has been resident for 3 years, and
both tenant and shareholder wish to extend the lease.
    
14. A Qualifying Shareholder may apply to the Board to have any of these
conditions waived.
