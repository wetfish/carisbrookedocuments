% MANAGEMENT GUIDELINES
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

## 1\. ORGANISATION

### A\. Company Title

The building known as Carisbrooke, 11 Springfield Avenue Potts Point is
a company title building, which means that the company Carisbrooke Homes
Units Pty Limited (ABN 46 000 206 851) ("Carisbrooke" or "the Company")
is the legal owner of the entire building and property, including each
apartment. This differs from buildings that are strata titled, where a
company/corporation owns the common property only and each apartment is
legally owned by the individual owner.

The members of Carisbrooke own shares in the Company. The total number
of shares of the Company is 66,100 divided into 28 groups of shares,
representing each apartment. Ownership of a group of shares entitles a
member to various rights and imposes various obligations on the member.
The most important right that share ownership confers is the right to
live in the apartment to which that member's group of shares relates.

The Memorandum and Articles of Association (or "Constitution") of
Carisbrooke sets out the objectives and powers of the Company, as well
as the basic rights, obligations and rules for living in Carisbrooke.
The Constitution has been amended on various occasions over the years.

### B\. The Board of Directors ("Board")

The Directors of the Company, are elected by the shareholders at each
Annual General Meeting ("AGM"). The Directors are responsible for
managing the affairs of the Company, and are empowered to do all things
that the Company can do under its constitution subject to the
Corporations Law. Although the constitution does provide for Directors'
remuneration, in practice Directorship is on a voluntary, unpaid basis.

The Directors of Carisbrooke, like Directors of companies in general,
have a duty to act in the best interest of the company according to its
Constitution. The practical application of this duty involves oversight
of general repair and maintenance issues, management of more significant
capital expenditure items (that require sinking fund special levy
contributions), managing the finances and insurance of the Company, and
setting more policy-oriented matters such as House Rules, renovation or
leasing policy guidelines, etc.

Directors need to be aware that the development and setting of policy
should be done in accordance with the Company's Constitution.

The current Board of Directors is set out in the Preferred Tradespeople
and Contacts List.

### C\. The Managing Agent

A Managing Agent is appointed by the Company/Board to carry out the
functions of the Board as its agent. In practical terms this primarily
means keeping the official records and financial accounts of the
Company, attending to general administrative and clerical matters, and
handling routine repairs and maintenance. The current Managing Agent is
Alldis & Cox (Coogee) Pty Limited ("Alldis & Cox") who was appointed
effective 1^st^ July, 2002. The specific services provided by Alldis &
Cox are set out in their quotation letter dated 21^st^ May, 2002
(available upon request).

Directors need to be aware that the Managing Agent only acts as an agent
of the Board/Company and the overall responsibility for management of
Carisbrooke rests with the Directors.

## 2\. CONTACT DETAILS

Various contact details are contained in the annexures, including for
the Managing Agent and the Board of Directors, as well as common
property tradespeople.

Initial contact by members and residents of Carisbrooke, should
**always** be made with the Managing Agent in relation to any matters
affecting the Company or the running of Carisbrooke. The Managing Agent
will then contact a Member of the Board if required. Only in the case of
an emergency should a member of the Board of Directors be contacted
directly by members or residents.

## 3\. HOUSE RULES

A list of House Rules is contained in the annexures. This list will be
updated as required and a copy will be sent to all shareholders as
required.

The purpose of the House Rules is to provide a method of regulating life
in Carisbrooke for the enjoyment of all residents. Although the list
appears extensive it is essentially similar to the standard strata title
by-laws for residential strata title properties.

## 4\. REPAIRS AND MAINTENANCE - PROCEDURES

The Managing Agent handles routine repairs and maintenance matters, with
oversight by the Board.

The procedure for the handling of repairs and maintenance matters is as
follows:

-   Non-Urgent Repairs and Maintenance
    
    -   By definition these matters should only be dealt with during
        normal business hours;

    -   Contact the Managing Agent - with details of the problem, and
        contact details (name, address, phone number) of the person
        making the request;

    -   The Managing Agent will advise the Board of the request;

    -   The Board will consider the matter (generally at the next
        meeting of the Board) or via a circular meeting (eg by email).

-   Urgent Repairs and Maintenance
    
    -   If the matter arises during normal business hours, the Managing
        Agent should be contacted first, with details as noted above.
        The Managing Agent will promptly arrange for a tradesperson to
        attend to the matter, usually selected from a list of preferred
        tradespersons given to the Managing Agent (refer to annexure
        list of contact details). The Managing Agent will have
        discretion to have the matter attended to without reference to
        the Board, depending on its urgency.

    -   If the matter arises outside or normal business hours (including
        weekends), the appropriate tradesperson from the Preferred
        Tradespeople and Contacts List should be contacted directly.
        Again, the person making the request should provide full details
        of the problem and their name and contact details.

    -   The Company will from time to time have certain incumbent
        tradespeople (usual plumber, electrician etc) in addition to
        specific tradespeople for certain equipment (eg Electra Lifts
        who hold the maintenance contract for the elevator).

## 5\. REPAIRS AND MAINTENANCE - COSTS

### Levies

Repairs and maintenance that are the Company's responsibility are paid
for by levies from members. Levies are based on the proportional
shareholding of each member (Article 16) and apply to rates and taxes,
repairs and maintenance of external areas and those areas in "common
use", compliance with statutory orders, etc. It is possible to
alter/amend the shareholding by a special resolution at a general meeting
in accordance with Articles 7(a), 35, 36 &amp; 39.

### Allocation of Cost between the Company and Shareholders

It is not always clear whether the Company or an individual member is
responsible for paying for certain items of repairs and maintenance.
Strata title legislation can provide some conceptual guidance, however
strata title buildings explicitly differentiate between "private lot
property" and "common property", whereas no such explicit distinction
exists with company title buildings (the Company legally owns the entire
building, including each apartment). The allocation of responsibility
for costs should be made according to the Constitution of the Company as
far as possible, and on the basis of fairness and equity.

### What does the Constitution of The Company say?

The Memorandum of Association of the Company sets out the objectives of
the Company, which includes dealing with the property of the Company (ie
the Carisbrooke land and building) as the Company thinks fit, and
installing and maintaining of conveniences to keep the building suitable
and available for occupation by the members of the Company.

The Articles of Association set out how the Company functions, including
the rights and obligations of the shareholders. They state that share
ownership gives the full right to occupation of the apartment in respect
of which such group of shares is held. This right is subject to a number
of conditions, such as the member being responsible at their own expense
for keeping the interior of an apartment and the fixtures therein clean
and in good order and condition. This implies that the Company is
responsible at its expense for keeping only the exterior to apartments
in good order. The question arises as to the proper boundaries for cost
allocation purposes.

The Company has the right to enter an apartment in connection with the
inspection and repair of utility services. The Company can raise general
levies for, among other things:

-   External painting, repairs and maintenance to keep the building in
    good order and condition;

-   Repairs and maintenance of such internal and external passages and
    rooms as are in common use and the replacement of articles in common
    use;

-   All charges and outgoings, which the Board in its discretion
    considers expedient to maintain or enhance the value of
    the property.

### Conceptual guidance from Strata Legislation

The duties of an Owners Corporation under strata legislation, include
keeping in a state of good and serviceable repair:

-   The common property;

-   All chattels, fixtures and fittings (including elevators and
    fire escapes) related to the common property or its employment;

-   All apparatus, equipment and services (including pipes, wires,
    cables and ducts) including those within a lot;

-   The cost of this work can be for the Owners Corporation expense if
    it cannot be recovered from some other person.

In general, the Owners Corporation is responsible for looking after
common property and is not required to maintain any service that is used
by only one lot.

An individual lot owner is generally obliged to maintain a lot and any
services that serve that lot exclusively, even if the service passes
through common property. As such, the Owners Corporation may recover
costs for repairs carried out substantially for the benefit of some, but
not all, of the lots affected by it. There is no clear guidance on what
constitutes a "substantial benefit" - one suggestion is that this is
where the expense incurred provides a significant benefit to one lot
owner but is of little or no benefit to the other lot owners or the
Owners Corporation in general. Therefore repairs to the outside of a
particular lot generally do not fall within this category because they
provide a benefit to other lot owners. In conclusion, there is no clear
definition of what constitutes a "substantial benefit" and accordingly
each situation needs to be assessed on its merits.

### Insurances

Some guidance can also be taken from the building insurances.
Carisbrooke's insurers, CGU Insurance, provide insurance for accidental
damage/loss to the "Building" and "Common Property". (They do not provide
cover for gradual deterioration and wear and tear). The "Building" is
defined as the building and any improvements, services (electricity,
water etc), any item built in, or fixed to, or on, the building, and
anything permanently built, constructed or installed on the property
(including items built, constructed or installed permanently by an owner
of a unit).

Therefore Building Insurances would generally cover kitchen cupboards and
built-in wardrobes, even though an individual member has paid for the
items. This is why it is necessary for any renovations in excess of
$5,000 to be advised to the Company so that the cost of the renovations
be included in the insured value of the Building. "Common Property" is
defined by reference to strata title concepts.

### Recommended approach for Carisbrooke re Cost Allocation

In the following discussion, strata title concepts of "Common Property"
will be used to indicate areas of responsibility (financial and
otherwise) for the Company. (In the Articles the concept of "common
areas" is used).

It is recommended that a conceptual approach similar to strata title
legislation be used to allocate costs for repairs in Carisbrooke.
Damage/repairs to Common Property is the responsibility of the Company.
Damage/repairs to individual apartments is the responsibility of a
member. The boundary between "common areas" and an apartment "interior",
is generally the surface of walls. Wall coverings such as wallpaper,
paint, carpet etc are usually NOT considered part of Common Property -
rather they are considered contents of apartments and, for example, may
not be covered by Building Insurance (ie not considered to be Common
Property).

Referring again to insurances, when accidental damage/loss to the
Building/Common Property occurs (eg from hail/storm) the Company is
responsible for repairing the Building/Common Property, and can claim
out of the Building insurances. The Company should be insured for damage
to fixtures such as kitchens.

As noted above, Building Insurance does not cover the cost of repairing
gradual deterioration/wear and tear. Nor does it cover accidental damage
caused by water entering the building because of structural defect,
faulty design or workmanship when the building was constructed. It is
not certain whether accidental damage caused by gradual
deterioration/wear and tear (eg a burst old pipe or tank causing
flooding) will be covered by Building insurance, although we note that
there is generally a requirement for a building to be properly
maintained in order for its insurances to be effective. Therefore, the
Company is responsible for the cost of both day-to-day
maintenance/repair, and possible accidental damage to the
Building/Common Property caused by the failure of items that have
deteriorated.

### Fairness & Equity

A concept of fairness and equity should apply, in that the Company
should not be responsible for damage caused by a member. The question
could be asked: "has a shareholder contributed to the damage actively,
or by their or her inaction?" Note that if the action or inaction of a
member is responsible for damage, the cost of reimbursement should be
borne by the member contributing to the damage.

### Examples of Expense Allocation

Following are some situations involving Carisbrooke:

-   **Windows:** Windows have always been considered an expense of the
    Company. They are considered part of the "common areas" because
    their exterior is outside of an apartment. Although the substantial
    benefit of their repair may immediately accrue to an individual
    apartment, ongoing water penetration could also impact on the rest
    of the building. Where an apartment is being renovated, members have
    usually paid for new windows themselves, since a window upgrade
    might not be necessary. If a member is renovating windows where
    repair is otherwise necessary, then the Company should not be
    responsible for any costs in excess of the minimum cost of repairing
    the window to the standard of the building.

-   **Damage to building roof and apartment ceilings:** Damage to the
    roof and ceilings has occurred from hail damage (Apt.28), and worn
    roof-top membranes (Apt.26). This has generally been considered the
    Company's responsibility to repair because it involves damage to
    Common Property. Again, the Company *should* only be responsible for
    repairing the Common Property, not the contents of apartments.

-   **Damage caused by Members:** Where damage is caused by the actions
    of members, eg overflowing baths, or by standing by while a
    situation needing repair gradually deteriorates, etc, it is the
    responsibility of the member causing damage to compensate the
    injured party. If the damage is to the Common Property, then the
    Company should be compensated, if damage is to the personal contents
    of other members, this is a matter for the two members and not the
    Company.

-   **Rising damp in ground floor and lower ground floor apartments:**
    The costs incurred in relation to this item have been paid by the
    Company, and have not been recoverable under Building insurances.
    The rationale for the Company paying is that the rising damp impacts
    on Common Property (ie walls of the building). They are not
    considered recoverable under Building Insurances because of specific
    policy exclusions such as "water entering because of structural
    defect", "flood" (broadly defined) etc. As the Company has taken
    reasonable steps (and expense) to rectify the problem of rising
    damp, it is suggested that any future re-occurrence of rising damp
    should be for the expense of the individual members under the
    principles of fairness, equity and "buyer beware". We note again the
    principle under strata legislation that costs may be recovered for
    repairs carried substantially for the benefit of some, but not all,
    of the lots affected by it. It may also be noted that purchasers of
    the relevant units may have been on notice that they were or would
    be affected by rising damp and this might have been factored into
    their purchase price.

-   **Electrical wiring within an apartment:** Upgrading of electrical
    wiring has generally been for the cost of the member, because it is
    considered a service to the particular apartment (refer to plumbing
    below), and because it is considered part of the members'
    responsibility. Note that in describing the items of expenditure
    subject to general levies, the Articles refer to "the amount payable
    for electric light and power for outside lighting and in those
    portions of the building which are in common use".

-   **Plumbing:** Where plumbing work is required to common pipes (eg on
    the outside of the building) the Company has been responsible for
    repairs and maintenance and the associated costs. Where pipes are
    inside an apartment (eg bathroom sink pipes, toilet etc), the member
    has been held responsible. This is consistent with the strata title
    principle that an individual lot owner is generally obliged to
    maintain a lot and any services that serve that lot exclusively
    (even if they pass through common property). There is a grey area of
    responsibility in relation to any pipes that may be inside a wall or
    underneath the floorboards of an apartment, and that may have been
    constructed by an individual member (refer further below). We do
    note, however, that piping generally is insured under Building
    Insurance.

-   **Plumbing extensions arising from renovations:** This situation was
    considered in August - October, 2000, in relation to Apartment 12
    renovations and relocation of kitchen plumbing from the rear of the
    apartment to the front. Legal advice from Blessington Judd
    at the time indicated that works done
    pursuant to a properly approved development application will, after
    completion, restrict a shareholder's liability to maintenance unless
    the liability is expressly extended to damage from water pipes to
    other company property as part of the approval process. Blessington
    Judd recommended that Board approval be given conditionally (re
    future repairs & Maintenance) and that the member enter into a deed
    with the Company regarding future liability for damage, which would
    also contain a restriction on transfer of shares unless the
    purchaser enters into a similar deed.


## 6\. CONTENTS INSURANCE

In order to reduce the potential exposure of the Company to claims from
individual members who may suffer loss as a result of damage to their
apartments contents, it is recommended that the House Rules expressly
state that it is an obligation of each member and resident to take out
full contents insurance and that the Company expressly disclaims any
liability in relation to damage to contents of members/residents.
Consideration may also be given to amending the Articles of Association
along similar lines although we note that this might require unanimous
approval of members.
