% BICYCLE POLICY
---
date: February 2019
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

The following outlines the company's
position on bicycles, their movement and storage within the building;

1.  Bicycles are not permitted in the lift or the roof top or to enter/exit from
    the main front doors

2.  Bicycle owners are to enter from Earl Lane and may use the stairs to access
    their unit or;

3.  Bicycles can be stored between Carisbrooke and Springfield Lodge, access to
    this area is under the stairs at the basement level

4.  Tenants choosing to store their bicycles between the buildings can apply to
    the board for reimbursement for a rain coat for their bicycle.

With respect to (1), the Board has resolved that as from September 1 2013 a fine
of $250 will apply to anyone contravening this rule.

Bicycle owners are asked to be mindful of making any undue noise on entering and
leaving the building through the back passageway and gate and to please ensure
that the back door and gate are locked after using them.
