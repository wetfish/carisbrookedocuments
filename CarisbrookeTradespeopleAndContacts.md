% Preferred Tradespeople and Contacts List
---
date: May 2019
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=0.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}}
- \subsectionfont{\color{orange}}
- \sectionfont{\centering}
---

Primary Contacts
----------------

----------------------------------------------------------------------
Responsibility          Name                       Contact Details    
----------------------- -------------------------- -------------------
Managing Agent          Alldis & Cox               Ph. (02) 9326 4488

Rooftop Party Bookings  Susan Wheeldon             Ph. 0431 747 145

Plumbing                Susan Wheeldon             Ph. 0431 747 145

Phone & Intercom        Andrew Smith               Ph. 0418 210 759

Building Electrical     Andrew Smith               Ph. 0418 210 759

Locked out ??           Andrew Smith               Ph. 0418 210 759

                        Bronwyn Davies             Ph. 0407 212 943

                        Miriam Chapman             Ph. 0455 583 683
----------------------------------------------------------------------


Recommended Tradespeople
------------------------

------------------------------------------------------------------------
Purpose/Area            Name                       Contact Details    
----------------------- -------------------------- -------------------
Cleaner                 Ann Arnold                 Ph. 0402 018 506

Plumber                 Mr Washer Plumber &        Ph. 1300 679 274
                        Electrician To The Rescue

Electrician             Patrick Chapman            Ph. 0415 660 911

Locksmiths              Bells Lock Smiths          Ph. (02) 9357 2333

Lift (Emergency)        Electra Lift               Ph. (02) 9667 4826

Fire Equipment          Fire Safe Maintenance      Ph. (02) 9552 1904

Structural Engineers    Constin Roe Consulting     Ph. (02) 9251 7699
------------------------------------------------------------------------

Company Directors
-----------------

------------------------------------------------------------------------
Name                    Division  Units Represented Contact Details    
----------------------- --------- ----------------- -------------------
Andrew Smith            One       1 - 4 & 1A        Ph. 0418 210 759

David Waite             Two       5 - 7             Ph. 0414 385 162

Paul McKeon             Three     8 - 10            Ph. 0414 596 266

Susan Wheeldon          Four      11 - 14           Ph. 0431 747 145

Tara Czinner            Five      15 - 17           Ph. 0401 808 038

Bronwyn Davies (Chair)  Six       18 - 20           Ph. 0407 212 943

Brad Buckley            Seven     21 - 23           Ph. 0439 524 222

Jason Prior             Eight     24 - 25           Ph. 0408 463 202

Jock Pharey             Nine      26 - 27           Ph. 0419 703 814

Greg Bayles             Ten       28                Ph. 0407 001 947
------------------------------------------------------------------------
