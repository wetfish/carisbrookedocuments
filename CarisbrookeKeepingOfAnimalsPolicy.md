% KEEPING OF ANIMALS POLICY
---
date: September 2017
output:
  pdf_document:
    latex_engine: xelatex
geometry: margin=2.5cm
header-includes:
- \renewcommand{\familydefault}{\sfdefault}
- \usepackage{helvet}
- \usepackage{xcolor}
- \usepackage{sectsty}
- \subsubsectionfont{\color{cyan}\centering}
- \subsectionfont{\color{orange}\centering}
- \sectionfont{\centering}
---

The Board may at its discretion permit the keeping of certain dogs, cats
and birds hereinafter referred to as a "permitted animal" subject to the
following conditions:-

1.  A resident seeking to keep an animal must apply in writing to the
    Board giving a clear description of the animal on the prescribed
    form and receive in return written approval from the Board before
    keeping a permitted animal.
    
2.  Multiple pets are at the Board's discretion.
    
3.  Permission, if granted, is for the lifespan of a permitted animal or
    to the end of its tenure within a Lot.
    
4.  A permitted animal must be carried or on a short leash when on
    Common Property.
    
5.  A permitted animal must be kept within the boundary of a Lot and if
    practicable be provided with a litter tray for animal waste, such
    tray must be kept within the boundary of the same Lot.
    
6.  The disposal of litter or animal faeces must not be made by flushing
    the litter down the toilet but must be bagged and wrapped and placed
    in the bins in garbage area by the owner of the permitted animal.
    Placing the litter in the garbage bin on the roof is not permitted.
    
7.  If a permitted animal defecates or urinates upon or otherwise
    damages Common Property its owner shall bear the cost and
    responsibility of removal of the faeces and repair of damage.
    
8.  Approval to keep a permitted animal may be revoked if the animal is
    deemed by the Board to be causing damage, noise, offensive odours,
    fleas or other vermin infestation or is kept in breach of the terms
    of this by-law.
    
9.  **Permitted animals are not allowed on the rooftop**.
    
10. Owners of animals will abide by all City of Sydney by-laws and
    regulations, see
    <http://cityofsydney.nsw.gov.au/Residents/Animals/Default.asp>

In making its decision whether to permit the keeping of an animal within
a lot, the Board will refer to the RSPCA Apartment Guide for keeping of
dogs and other information relating to the suitability of breeds for
apartment living, such as, but not restricted to:
<http://www.whosyadoggy.com/dog-breeds-for-apartments-low-exercise-needs.html>

\newpage

### Carisbrooke Home Units Pty Ltd \
11 Springfield Avenue, Potts Point

## APPLICATION TO KEEP AN ANIMAL

-------------------------- ---------------------------------------------
\break

NAME OF APPLICANT:         _________________________________
\break

UNIT NUMBER:               _________________________________
\break

PHONE CONTACT:             _________________________________
\break


OWNER/TENANT (SEE NOTE):   _________________________________
\break


NAME OF ANIMAL:            _________________________________
\break


TYPE OF ANIMAL:            _________________________________
\break


AGE:                       _________________________________
\break


SEX:                       _________________________________
\break


WEIGHT:                    _________________________________
\break


COLOUR:                    _________________________________
\break


REG NO:                    _________________________________
\break

-------------------------- ---------------------------------------------

&nbsp;

I have read and understand the conditions upon which permission to keep an
animal may be granted and undertake to abide by those conditions. I further
understand that this form is an application to keep a single animal for the
lifespan of that animal and that permission may be revoked if the animal is kept
in breach of any by-law.

&nbsp;

SIGNED: _________________________________ DATE: _______________________________

&nbsp;

*NOTE*: TENANTS WHO WISH TO KEEP AN ANIMAL MUST HAVE WRITTEN PERMISSION OF THEIR
LETTING AGENT OR THE OWNER OF THE UNIT.

---

To be completed by the Managing Agent or Member of the Board:

&nbsp;

APPROVAL GIVEN:  &nbsp;&nbsp;&nbsp; YES ____  &nbsp;&nbsp;&nbsp; NO ____

&nbsp;

SIGNED: _________________________________ DATE: _______________________________
