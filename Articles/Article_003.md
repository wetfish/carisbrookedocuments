3. The shares of the Company shall be under the control of the Directors
who may issue and allot them either as one class or several classes and
at such time or times and in such manner in all respects as the
Directors shall think fit and the Directors may attach to the shares
comprised in any class any preference or guaranteed right to any
dividend either cumulative or payable out of the profits of any
particular year or years.
