57. The continuing Directors may act notwithstanding any vacancy in
their body and notwithstanding that such continuing Directors do not
form a quorum.
