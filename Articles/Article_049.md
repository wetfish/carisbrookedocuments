49. Save as hereinbefore expressly provided no person other than a
member duly registered and who shall have paid everything for the time
being due from them and payable to the Company in respect of their share
shall be entitled to be present either personally or by proxy or by their
attorney at any General Meeting of the Company.
