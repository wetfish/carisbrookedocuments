56. Any casual vacancy occurring in the Board of Directors may at any
time be filled by the Directors by the appointment of some properly
qualified member; but every person so chosen shall retain office only
until the next Ordinary General Meeting of the Company when they shall be
eligible for re-election.
