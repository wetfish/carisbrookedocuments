41. The Chairperson may with the consent of the meeting adjourn the same
from time to time and from place to place but every adjourned meeting
shall be treated as a prolongation only of the original meeting and
shall only be competent to transact business which might properly have
been transacted at the original meeting.
