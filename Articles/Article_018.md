18. For the purposes of the provisions of these presents relating to
forfeiture of shares the sum payable upon allotment in respect of a
share shall be deemed to be a call payable upon such share on the day of
allotment.
