103. Any notice so sent by email to a Member shall be deemed to have been served
on the date that the email was sent, provided the sender does not receive an
electronic notification of unsuccessful transmission within 24 hours.
