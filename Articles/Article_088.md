88. Any general meeting may resolve that any moneys investments or other
assets forming part of the undivided profits of the Company standing to
the credit of the reserve fund or in the hands of the Company and
available for dividend or representing premiums received on the issue of
shares and standing to the credit of the share premium account be
capitalised and distributed amongst such of the shareholders as would be
entitled to receive the same if distributed by way of dividend and in
the same proportions on the footing that they become entitled thereto as
capital and that all or any part of such capitalised fund be applied on
behalf of such shareholders in paying up in full either at par or at
such premium as the resolution may provide any unissued shares or
debentures or debenture stock of the Company which shall be distributed
accordingly or in or towards payment of the uncalled liability of any
issued shares or debentures or debenture stock and that such
distribution or payment shall be accepted by such shareholders in full
satisfaction of their interest in the said capitalised sum.
