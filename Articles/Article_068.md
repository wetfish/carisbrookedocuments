68. The Directors shall at the first meeting of the Directors after the
yearly election elect a Chairperson of Directors for the ensuing year and
if the position shall become vacant after any meeting a Chairperson for
the rest of the year shall be elected at the first meeting of the
Directors after notice of the vacancy has been given to the Directors.
