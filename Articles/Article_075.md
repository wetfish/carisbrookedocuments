75. A resolution determined on without any meeting of the Directors or a
committee or sub-committee and evidenced in writing under the hand of
all the Directors or of all the members of a committee or sub- committee
respectively shall be as valid and effectual as a resolution duly passed
at a meeting of the Directors or of such committee or sub-committee
respectively duly called and constituted.
