2. The Company is to be a proprietary company and accordingly the
following provisions shall have effect, namely:-
    
    a. The number of members for the time being of the Company
    (exclusive of persons who are in the employment of the Company and
    of persons who having been formerly in the employment of the Company
    were while in that employment and have continued after the
    determination of that employment to be members of the Company) is
    limited to fifty \(50), and
      
    b. Any invitation to the public to subscribe for any shares or
    debentures or debenture stock of the Company or to deposit money
    with the Company for fixed periods or payable at call whether
    bearing or not bearing interest is hereby prohibited, and
      
    c. The right of transfer of shares shall be restricted as
    hereinafter provided.
