24. For the purpose of enforcing such lien the Directors may sell the
shares subject thereto in such manner as they shall think fit but no
sale shall be made until:-
    
    a. The time for such payment fulfilment or discharge as aforesaid
    shall have arrived, and
    
    b. Notice in writing of the intention to sell shall have been
    served on such member or their representatives, and
    
    c. Default shall have been made by them in payment
    fulfilment or discharge of such debts liabilities or engagements
    for fourteen \(14) days after such notice.
