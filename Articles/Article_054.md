54. The remuneration of the Directors shall be determined by the
shareholders in Ordinary General Meeting. Notice of any proposed
alteration in such remuneration shall be given in the notice convening
the meeting. Such remuneration shall be divided among the Directors in
such proportions and in such manner as the Directors may determine.
