62. A Director may at any time give one \(1) month's notice in writing to
the Company of their desire to resign and at the expiration of such notice
their office shall be vacated.
