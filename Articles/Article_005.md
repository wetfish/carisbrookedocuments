5. The joint holders of a share shall be severally as well as jointly
liable for the payment of all instalments and levies and calls due in
respect of such share.
