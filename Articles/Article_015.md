15. Once in each half year or at such other interval as the Company by
ordinary resolution may from time to time determine the Directors shall
have the right to make a levy on the shareholders in the Company
provided that such a levy shall not exceed the sum required to meet all
expenses charges and outgoings set out hereunder and so that each
shareholder shall only be required to contribute to such levy on the
basis of the proportion of the number of shares held by such shareholder
in the Company at the time such levy is made to the total number of
issued shares of the Company at that time or such other basis as the
Directors from time to time may determine:-
    
    a. All rates and taxes.
    
    b. The amount payable for charges imposed upon the said property
    by any properly constituted body.
    
    c. Insurance premiums for insurance of the said building and of
    such of the contents as are the property of the Company against loss
    by fire storm or tempest, premiums for workers compensation,
    fidelity guarantee, superannuation or such other risks as the
    Directors may from time to time determine.
    
    d. The costs of such external painting, repairs and maintenance as are
    necessary to keep the building in good order and condition.
    
    e. The cost of colouring, repairs, and maintenance of such
    internal and external passages and rooms as are in common use and
    cleaning thereof and the proper maintenance of gardens lawns paths
    and grounds and the replacement of articles in common use.
    
    f. The carrying out of any requirement (apart from structural
    alterations) of any local or statutory authority except in relation
    to any particular flat.
    
    g. The amount payable for electric light and power for outside
    lighting and in those portions of the building which are in common
    use.
    
    h. Expenses of carrying on the Company including Directors' fees
    accountancy and legal charges.
    
    i. Any items of expenditure carried forward from the previous
    half-year or period.
    
    j. All charges and outgoings which the Board in its discretion
    considers expedient to maintain or enhance the value of the
    property.
        
    Upon any such levy being made in accordance with this Article notice
    in writing shall be given to the holder of each group of shares
    addressed to them at their registered address and the amount of such
    levy shall become due and payable fourteen days thereafter and may
    be recovered by action in any court of competent jurisdiction.
    Should the holder of any group of shares fail to pay the amount of
    such levy within such period of fourteen days a second notice in
    writing shall be forwarded to such holder as hereinbefore provided
    and unless such holder shall pay the amount of such levy within such
    additional period of fourteen days the Directors (without prejudice
    to any other right that the Company may have against the shareholder
    for the recovery of such levy) may enter into possession of the flat
    which the holder has the right to occupy and receive the rents
    therefrom until the amount of such levy is fully satisfied. The
    Directors shall also have a lien upon the shares of the holder until
    such levy is fully satisfied in similar manner as to the liens
    attaching in case of non-payment of calls.
