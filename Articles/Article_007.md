7. Save as herein otherwise provided, the Company shall be entitled to
treat the registered holder of any share as the absolute owner thereof
and accordingly shall not, except as ordered by a Court of competent
jurisdiction or as by statute required, be bound to recognise any
equitable or other claim to or interest in such share on the part of any
other person.
