107. The common seal of the Company may be used by the authority of the
Directors or some person to be appointed by the Directors provided that
one at least of the persons authorising such use signs every instrument
to which such seal is affixed.
