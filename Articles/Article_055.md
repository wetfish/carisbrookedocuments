55. No person shall be qualified to be a Director if they are not a holder of at
least 1100 shares in the Company, provided that a person who is a holder jointly
with another or others of at least 1100 shares in the Company shall be qualified
to be a Director.
