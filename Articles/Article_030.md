30. The Directors shall have the same right to refuse to register a
person entitled by transmission to any shares or their nominee, as if he
were the transferee named in an ordinary transfer presented for
registration.
