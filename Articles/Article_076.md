76. The management of the business of the Company shall be vested in the
Directors and the Directors may exercise all such powers and do all such acts
and things as the Company is by its Memorandum of Association or otherwise
authorised to exercise and do and are not hereby or by Statute directed or
required to be exercised or done by the Company in General Meeting but subject
nevertheless to the provisions of the Corporations Act and of these presents and
to any regulations not being inconsistent with these presents from time to time
made by the Company in General Meeting; provided that no such regulation shall
invalidate any prior act of the Directors which would have been valid if such
regulation had not been made.
