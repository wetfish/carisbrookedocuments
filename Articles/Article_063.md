63. Unless otherwise determined in any special case by a Resolution of a
General Meeting, the office of a Director shall be ipso facto vacated in
any of the following cases:-
    
    a. Subject to Article 55 hereof if they cease to hold the
         required qualifications.
    
    b. If they make any composition with their creditors or becomes
         bankrupt.
    
    c. If they are found to be a lunatic or becomes of unsound mind or of
         such infirm health as to be incapable of managing their affairs.
    
    d. If they are convicted of a felony.
    
    e. If they are absent from the meetings of the Directors during a
         period of six \(6) calendar months without special leave of
         absence from the Board of Directors.
