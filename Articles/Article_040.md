40. If within one \(1) hour from the time appointed for the meeting a
quorum of members is not present the meeting if convened upon the
requisition of members shall be dissolved. In any other case it shall
stand postponed to the same day in the next week at the same time and to
the same place; and at such postponed meeting the business shall be
transacted whatever the number of members present.
