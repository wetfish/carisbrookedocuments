74. Every committee and sub-committee shall keep minutes of their
proceedings and shall from time to time report on them to the Board of
Directors.
