106. Receipts for money payable to the Company may be signed by one
Director or by the Secretary or by any other person authorised by the
Board to receive money either generally or any particular sum of money
on behalf of the Company and such receipt shall be deemed to be valid.
