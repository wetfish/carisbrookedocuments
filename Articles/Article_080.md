80. The Directors shall cause minutes to be duly written and circulated to all
members:-
    
    a. Of all appointments of officers.
    
    b. Of the names of the Directors present personally or by proxy at
         each meeting of the Board and of any committee of Directors.
    
    c. Of all orders made by the Directors and committees of
         Directors.
    
    d. Of all resolutions and proceedings of general meetings and of
         meetings of the Board and committees.

And any such minutes of any meeting of the Board or of any committee or of the
Company will be submitted to the next meeting for approval and for subsequent
circulation to the members.
