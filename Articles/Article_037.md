37. Notice of a general meeting:

    a. must be given to all Members at least 21 days' prior to the date of the meeting, or as otherwise in accordance with the notice requirements of Section 249H of the Corporations Act;

    b.	must be served on all Members in accordance with articles 100 to 104;

    c. must:
        
        i.   set out the place, date and time for the meeting; and
        
        ii.  state the general nature of the meeting's business; and
        
        iii. if a special resolution is to be proposed at the meeting--set out an
        intention to propose the special resolution and state the resolution; and
        
        iv.  if a member is entitled to appoint a proxy--contain a statement setting
        out the following information:
            
            1. that the member has a right to appoint a proxy;
            
            2. whether or not the proxy needs to be a member of the company;
            
            3. that a member who is entitled to cast 2 or more votes may appoint 2
               proxies and may specify the proportion or number of votes each proxy
               is appointed to exercise; and
        
        v.   comply with any other disclosure requirements of Section 249L of the Corporations Act.  
