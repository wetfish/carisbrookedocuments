36. The Directors may, whenever they think fit, convene an
extraordinary meeting and they shall, on the requisition of members of
the Company representing not less than one-tenth of such of the
paid-up capital of the Company as at the date of requisition carries
the right of voting at general meetings, forthwith proceed to convene an
extraordinary meeting of the Company and in the case of such
requisition the following provisions shall have effect:-
    
    a. The requisition must state the objects of the meeting and must
    be signed by the requisitionists and deposited at the office and may
    consist of several documents in like form each signed by one or more
    of the requisitionists.
    
    b. If the Directors do not proceed to cause a meeting to be held
    within twenty-one \(21) days from the date of the requisition being
    so deposited, the requisitionists or any of them representing more
    than one-half of the voting rights of all of them may themselves
    convene the meeting but any meeting so convened shall not be held
    after three \(3) months from the date of such deposit.
    
    c. In the case of a meeting at which a resolution is to be proposed as a
    special resolution, the Directors shall be deemed not to have duly convened
    the meeting if they do not give such notice as is required by Section 249H
    of the Corporations Act.
    
    d. Any meeting convened under this Article by the requisitionists
    shall be convened in the same manner as nearly as possible as that
    in which meetings are to be convened by Directors.
    
    e. A requisition by joint holders of shares must be signed by all
    such holders.
