10. Subject to any special conditions on the allotment of shares all
calls on shares shall be made by and at the discretion of the Directors
and shall be payable at such times and places and by instalments or
otherwise as the Directors may appoint.
