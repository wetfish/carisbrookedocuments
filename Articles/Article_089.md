89. For the purpose of giving effect to any resolution under the last two
preceding Articles the Directors may settle any difficulty which may arise in
regard to the distribution as they think expedient and in particular may issue
fractional certificates and may fix the value for distribution of any specific
assets and may determine that cash payments shall be made to any members upon
the footing of the value so fixed or that fractions of less value than &pound;1
may be disregarded in order to adjust the rights of all parties and may vest any
such cash or specific assets in trustees upon such trusts for the persons
entitled to the dividend or capitalised fund as may seem expedient to the
Directors. Where requisite a proper contract shall be filed in accordance with
the Corporations Act and the Directors may appoint any person to sign such
contract on behalf of the persons entitled to the dividend or capitalised fund
and such appointment shall be effective.
