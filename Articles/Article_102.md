102. Any notice sent by post to a Member shall be deemed to have been served on
the fourth business day after the notice was posted in accordance with Section
72 of the Interpretation Act 1987 (NSW).
