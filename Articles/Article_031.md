31. The right to transfer shares in the capital of the Company is restricted.
The Directors may in their absolute discretion refuse to register any transfer
of shares by any shareholder to a transferee of whom they do not approve
provided that approval shall not be withheld in the case of a respectable and
responsible transferee fit to be an occupier of one of the aforesaid flats.
