64. No Director shall be disqualified by their office from holding any
office or place of profit under the Company or under any company in
which this Company shall be a shareholder or otherwise interested; or
from contracting with the Company either as vendor, purchaser or
otherwise nor shall any such contract or any contract or arrangement
entered into by or on behalf of the Company in which any Director shall
be in any way interested be avoided nor shall any Director be liable to
account to the Company for any profits arising from any such office or
place of profit or realised by any such contract or arrangement by
reason only of such Director holding that office or of the fiduciary
relations thereby established; but it is declared that the nature of their
interest must be disclosed by them at the meeting of the Directors at
which the contract or arrangement is first taken into consideration if
their interest then exists or in any other case at the first meeting of
the Directors after the acquisition of their interest. If a Director
becomes interested in a contract or arrangement after it is made or
entered into the disclosure of their interest shall be made at the first
meeting of the Directors held after they become so interested. A Director
may vote in respect of any contract or arrangement in which they are so
interested as aforesaid. A general notice that a Director is a member of
any specified firm or company and is to be regarded as interested in all
transactions with that firm or company shall be a sufficient disclosure
under this clause as regards such Director and the said transactions and
after such general notice it shall not be necessary for such Director to
give any special notice relating to any particular transaction with that
firm or company.
