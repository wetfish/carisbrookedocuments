105. All cheques or orders for payment shall be signed and all bills of
exchange promissory notes or other negotiable instruments shall be
accepted made drawn or endorsed and all bills of lading bonds or
warehousemen's certificates or the like instruments shall be endorsed
and all contracts and agreements not under seal shall be signed for and
on behalf of the Company by such person or persons as the Board shall
from time to time appoint. No other signatures or endorsement shall bind
the Company. Cheques or other negotiable instruments paid into the
Company's bankers for collection and requiring the endorsement of the
Company may be endorsed on its behalf by a Director or if so authorised
by the Board by the Secretary or any persons appointed by the Board.
