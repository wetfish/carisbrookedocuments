73. The Directors may appoint and remove committees and subcommittees of
the Directors for such purposes as they may think fit and may determine
and regulate their quorum and duties and procedure and may delegate to
them such of the powers of the Directors (except the power of making
calls) as the Board of Directors may think fit.
