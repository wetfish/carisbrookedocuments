53. The Directors of the Company shall be ten in number and shall be elected by
    the Shareholders and for the purpose of such election that Shareholders
    shall be allocated into the following divisions:
        
    -----------------------------------------
    Share Nos.       Unit Nos.  Division Nos.
    ---------------- ---------- -------------
    1     to 5600    1 - 4      1
    & 65551 - 66100  & 1A

    5601  to 11900   5 - 7      2

    11901 to 18450   8 - 10     3

    18451 to 25200   11 - 14    4

    25201 to 32150   15 - 17    5

    32151 to 39550   18 - 20    6

    39551 to 48000   21 - 23    7

    48001 to 53500   24 - 25    8

    53501 to 59450   26 - 27    9

    59451 to 65550   28         10
    -----------------------------------------
        
    Each Division shall elect a Director following notices in writing by the
    Board to the members of the Division of there being a vacancy on the Board
    for that Division. In the event of Division members nominating more than one
    Director, and each receiving equal votes from within the Division, the vote
    will be thrown open to all those present at the general meeting. When no
    nomination is forthcoming from the Division members, the General Meeting
    shall appoint a Director for the Division.
