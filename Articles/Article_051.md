51. The instrument appointing a proxy or attorney shall be deposited at
the Registered Office not less than twenty four \(24) hours before the
time appointed for the meeting at which the person named in such
instrument proposes to vote.
