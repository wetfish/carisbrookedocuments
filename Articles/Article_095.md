95. All dividends unclaimed for one year after having been declared may
be invested or otherwise made use of by the Board for the benefit of the
Company until claimed or until the same shall be dealt with in
accordance with the provisions of the "Unclaimed Moneys Act 1917" or
any amendment thereof or substitution therefor.
