33. The Company may by special resolution increase the capital of the
Company by the creation of new shares. Such new shares may be issued with
or without any special condition preference or priority either as to
dividend or capital or both or with any other special rights or
advantages as the Directors may think fit. In the absence of any special
condition or rights such new shares shall be held under the same
conditions as if they had been part of the original capital.
