93. Before recommending any dividend the Directors shall have power to
set aside out of the profits of the Company such sums as they think
proper as a reserve fund to meet contingencies or for special dividends
or for repairing improving and maintaining any of the property of the
Company and for such other purposes as the Directors shall in their
absolute discretion think conducive to the interests of the Company and
to invest the several sums so set aside upon such investments other than
shares of the Company as they may think fit and from time to time to
deal with and vary such investments and dispose of all or any part
thereof for the benefit of the Company and to divide the reserve fund
into such special funds as they think fit with full power to employ the
assets constituting the reserve fund in the business of the Company and
that without being bound to keep the same separate from the other assets
and also with full power with the sanction of the Company in general
meeting to distribute the whole or any part of such reserve fund as a
special dividend or bonus by issuing fully paid-up or partly paid-up
shares in proportion to the ordinary shares held by them in the Company.
