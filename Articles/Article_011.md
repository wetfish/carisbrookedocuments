11. Whenever any call is made fourteen \(14) days’ notice in writing
shall be sent to every person liable to pay the same specifying the time
and place of payment and to whom such call is to be paid.
