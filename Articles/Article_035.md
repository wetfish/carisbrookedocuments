35. A general meeting of the Company shall be held once at
least in every calendar year at such time not being more than fifteen
\(15) months after the holding of the last preceding general meeting and
at such place as may be determined by the Directors. Such general
meetings shall be called "ordinary meetings" and all other meetings of
the Company shall be called "extraordinary meetings".
