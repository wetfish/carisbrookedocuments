43. Minutes of the proceedings of every General Meeting shall be kept
and shall be signed by the Chairperson of the same meeting or by the
Chairperson of a succeeding meeting and the same when so signed shall be
conclusive evidence of all such proceedings and of the proper election
of the Chairperson.
