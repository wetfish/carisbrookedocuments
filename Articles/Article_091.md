91. Any dividend may be paid by cheque or warrant sent through the post
to the registered address of the member or person entitled thereto or in
the case of joint holders to any one of such joint holders at their
registered address or to such person and such address as the member or
person entitled or such joint holders as the case may be may direct.
