100. Each Member must provide the Company with an address for service, which may
be:
    
    a.	an Australian postal address; and/or
    
    b.  an email address.
