25. The net proceeds of any such sale shall be applied in or towards the
satisfaction of such debts liabilities or engagements and the balance
(if any) paid to such member or their representatives.
