82. Provided that where capital is paid up on any shares in advance of
calls upon the footing that the same shall carry interest such capital
shall not whilst carrying interest confer a right to participate in
profits.
