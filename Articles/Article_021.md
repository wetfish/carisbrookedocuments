21. Notwithstanding any such forfeiture as aforesaid all moneys which
were owing at the time of forfeiture whether for any call levy interest
or expenses and all interest and expenses to accrue in respect of such
call or levy after such forfeiture shall continue to be due from the
person who was liable to pay the same at the time of forfeiture or their
representative (but the Company shall not be entitled to recover by
action or otherwise more than the balance remaining due after deducting
the value ascertained by sale or otherwise of the shares so forfeited.)
