45. At any meeting unless a poll is demanded as hereinafter provided every
resolution shall be decided by a show of hands and on a show of hands every
member present in person or by a representative appointed pursuant to Section
249X of the Corporations Act shall have one vote. A declaration by the
Chairperson that a resolution has been carried or carried by a sufficient
majority or lost as the case may be and an entry to that effect in the Minute
Book of the Company shall be conclusive evidence of the fact without proof of
the number of votes recorded in favour of or against such resolution.
