52. Every instrument appointing a proxy shall be in the following form
or in such other form as the Directors may from time to time prescribe
or allow:-

\hfill \break

> I ______________ of ______________ the Registered holder of
> _________ shares of &pound;_____ each in the above Company hereby
> appoint ______________ of ______________ or failing them ______________
> of ______________ to be my proxy in my absence to vote in my name upon
> any matter proposed at the General Meeting of the above Company to be
> held on the ______________ day of ______________ next or at any
> adjournment thereof in such manner as such proxy shall think proper (and
> if expedient to demand a poll).
>
> As witness etc
>
> (sgd) Signature of the shareholder.

\hfill \break
