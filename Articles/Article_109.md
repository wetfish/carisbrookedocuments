109. If the Company shall be wound up whether voluntarily or otherwise
the liquidators may with the sanction of an extraordinary resolution
divide among the contributories in specie or kind any part of the assets
of the Company and may with the like sanction vest any part of the
assets of the Company in trustees upon such trusts for the benefit of
the contributories or any of them as the liquidators with the like
sanction shall think fit.
