1.  In these presents unless there be something in the subject or context
    inconsistent therewith:-
    
    -   "The Company" means the abovenamed Company.
    
    -   "Corporations Act" means the Corporations Act 2001 (Cth) or any
        statutory modification amendment or re-enactment thereof for the time
        being in force.
    
    -   "The Regulations" means and includes these Articles of
        Association and the Regulations of the Company from time to time
        in force.
    
    -   "The Office" means the Registered Office for the time being of
        the Company.
    
    -   "The Directors" means the Directors for the time being.
    
    -   "Shares" means the shares from time to time of the Company.
    
    -   "Member" means the holder of such shares.
    
    -   "The Register" means the register of members to be kept pursuant to
        Section 168 of the Corporations Act.
    
    -   "The Secretary" includes any person appointed to perform the
        duties of Secretary temporarily.
    
    -   "Dividend" includes bonus.
    
    -   "Special Resolution" and "Extraordinary Resolution" have the meanings
        assigned thereto respectively in Section 9 of the Corporations Act.
    
    -   "Board" shall mean a meeting of the Directors or the Directors assembled
        as a Board, as the case may be.
    
    -   Words importing the singular number only include the plural
        number and vice versa.
    
    -   Words importing persons include corporations.
