13. Any member may with the sanction of the Directors and upon such
terms as to payment of dividends or interest and otherwise as the
Directors may determine make payments in advance of calls.
