83. The Company in general meeting may declare a dividend to be paid to
the members according to their rights and interest in the profits and
may fix the time for payment. No larger dividend shall be declared than
is recommended by the Directors but the Company in general meeting may
declare a smaller dividend.
