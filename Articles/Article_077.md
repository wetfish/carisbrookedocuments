77. All acts done by the Directors shall notwithstanding that it be afterwards
    discovered that there was some defect in the appointment of any of them or
    that they or any of them were disqualified be as valid as if every such
    person had been duly appointed and was qualified to be a Director.
    
    \hfill \break
    77(a). Notwithstanding the general powers hereinbefore conferred upon them
    the Directors shall not sell, encumber, mortgage, lease or otherwise dispose
    of the property "Carisbrooke" or alter the main undertaking of the Company
    or a major part thereof without prior approval of the terms thereof by the
    holders of 75% of the shares of the Company for the time being issued at an
    Extraordinary General Meeting specially summoned for the purpose.

    \hfill \break
    77(b). For the purposes of the management of the property and the affairs of
    the Company, the Directors may, by resolution, make house rules, not
    inconsistent with these articles, and may vary and cancel those house rules.
    The house rules shall be binding upon the shareholders from time to time of
    the Company. Within 21 days of the Directors' resolution to make, vary or
    cancel a house rule, the Company must give each shareholder notice of the
    new house rule or of the cancellation at the address for that shareholder
    prescribed by Article 100.
