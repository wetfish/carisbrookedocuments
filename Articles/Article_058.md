58. At every Ordinary General Meeting one-third of the Directors or if their number
is not a multiple of three then the number nearest to but not exceeding
one-third shall retire from office. The Directors to retire shall be
those who have been longest in office since their last election. As
between Directors of equal seniority, the Directors to retire shall be
selected by lot. A retiring Director shall be at any time eligible for
re-election if properly qualified. The Company in General Meeting may
appoint a Director for any fixed or indefinite period and if thought fit
free from any liability to retire by rotation and any Director may be
removed by the Company in General Meeting.
