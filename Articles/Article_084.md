84. No dividend shall be payable except out of the profits of the
Company and no dividend shall carry interest as against the Company.
