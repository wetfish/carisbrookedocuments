101. The Company must serve any notice it is required to by these Articles or
the Corporations Act, or any other notice, on a Member, by:
    
    a.	ordinary post to the Australian postal address provided pursuant to
    Article 100; and/or
    
    b.	by email to the email address provided pursuant to Article 100.
