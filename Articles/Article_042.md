42. The Chairperson (if any) of the Directors shall preside as Chairperson at
every meeting of the Company. If there is no such Chairperson of Directors
or in case they are not present at the time for holding any meeting or
declines to take the chair then some one of the other Directors present
at the meeting (if any) shall preside at such meeting. In case no
Director is present or willing to take the chair then the members
present shall choose some one of their number to be Chairperson of such
meeting.
