28. The executors or administrators of a deceased member (not being one
of the several joint holders) shall be the only persons recognised by
the Company as having any title to the shares registered in the name of
such member and in the case of the death of any one or more of the joint
registered holders of any registered shares, the survivors shall be
the only persons recognised by the Company as having any title to or
interest in such shares.
