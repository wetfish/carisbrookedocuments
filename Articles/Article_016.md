16. Whenever any call, instalment of a call, or levy payable by any member shall
    not have been paid on the appointed day the Directors may at any time
    thereafter during such time as such call instalment or levy shall remain
    unpaid serve a notice requiring payment by a specified day and at the
    place where the calls or levies of the Company are usually made payable of
    the call instalment or levy so in arrear with or without interest thereon at
    the rate of interest quoted by the Company's bank for overdraft facilities
    with the bank from time to time from the day on which such call instalment
    or levy ought to have been paid. Such notice shall state that in the event
    of non-payment at the time and place appointed of the arrear with interest
    thereon (if any) together with such expenses (if any) as may be incurred in
    and about the collection or recovery of such call instalment or levy and
    interest then the shares in respect of which such call or levy was made will
    be forfeited without further notice.
