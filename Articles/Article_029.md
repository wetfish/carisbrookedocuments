29. Any person becoming entitled to shares in consequence of the death
or bankruptcy of any member, upon producing proper evidence of the
grant or probate or letters of administration or such other evidence
that they sustain the character in respect of which they propose to act
under this clause, or of their title as the Directors think sufficient,
may, with the consent of the Directors (which they shall not be under
any obligation to give), be registered as a member in respect of such
shares, or may, subject to the regulations as to transfers hereinbefore
contained, transfer such shares. This clause is hereinafter referred to
as "the transmission clause".
