94. Any one of the several persons who are registered as the joint
holders of any share may give effectual receipts for all dividends and
payments on account of dividends in respect of such shares.
