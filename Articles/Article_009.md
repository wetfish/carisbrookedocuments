9. If it be proved to the reasonable satisfaction of the Directors that
a certificate is lost worn out or defaced it shall be replaced by a new
certificate subject to such indemnity being given as the Directors may
from time to time prescribe.  Any renewed certificate shall be marked as
such.
