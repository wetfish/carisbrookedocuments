46. If at any meeting a poll is demanded by one member present in person
or by proxy or attorney and entitled to vote, the poll shall be taken at
such time and in such manner as the Chairperson shall direct; and in such
case every member at the taking of the poll either personally or by
proxy or by attorney under power shall have the number of votes to which
he may be entitled as hereinafter provided.
