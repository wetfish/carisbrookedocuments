39. Four \(4) members personally or by attorney or proxy present shall form a
    quorum. Save as is hereinafter provided no business shall be transacted at
    any General Meeting except the election of a Chairperson unless a quorum of
    members be present at the time when the meeting is prepared to proceed to
    business.
