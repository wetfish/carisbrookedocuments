27. Every instrument of transfer shall be presented to the Company duly
stamped and accompanied by the certificate of the share to be
transferred and such evidence (if any) as the Company may require to
prove the title of the transferor or their right to transfer the shares.
When registered the instrument of transfer shall be retained by the
Company but any instrument of transfer which the Directors may decline
to register shall on demand be returned to the person depositing the
same.
