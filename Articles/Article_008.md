8. Every member shall be entitled to a certificate under the Common Seal
of the Company signed by one \(1) Director and countersigned by the
Secretary specifying the share or shares held by them and the amount paid
up thereon; if two \(2) or more persons are registered as joint holders
of any share the certificate of any such share shall be delivered to the
person first named upon the Register unless all such joint holders shall
otherwise direct in writing.
