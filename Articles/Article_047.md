47. When there are joint registered holders of any share any one of such
persons may vote at any meeting either personally or by proxy in respect
of such shares as if they were solely entitled thereto and if more than
one of such joint holders be present at any meeting personally or by
proxy that one of the said persons so present whose name stands first on
the register in respect of such shares shall alone be entitled to vote
in respect thereof. Several executors or administrators of a deceased
member in whose name any share stands shall for the purpose of this
Article be deemed joint holders thereof.
