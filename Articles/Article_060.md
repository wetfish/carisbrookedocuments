60. If at any General Meeting at which an election of Directors ought to
take place, the place of any Director retiring by rotation is not filled
he shall, if willing, continue in office until the Ordinary Meeting
of the next year and so on from year to year until their place is filled
unless it shall be determined at such meeting on due notice to reduce
the number of Directors in office.
