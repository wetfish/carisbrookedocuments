61. No one (other than a retiring Director) shall be eligible to be a
Director unless notice in writing that they are a candidate for such office
shall have been given to the Company by two other members of the Company
at least seven \(7) days before the day of holding the meeting at which
the election is to take place.
