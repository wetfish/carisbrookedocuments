108. Every Director, manager, or officer of the Company or any person (whether
an officer of the Company or not) employed by the Company as auditor shall be
indemnified out of the funds of the Company against all liabilities incurred by
them as such Director, manager, officer or auditor in defending any proceedings,
whether civil or criminal, in which judgment is given in their favour, or in
which they are acquitted, or in connection with any application under the
provisions of the Corporations Act in which relief is granted to them by the
Court.
