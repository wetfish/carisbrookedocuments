23. The Company shall have a first and paramount lien upon all the
shares registered in the name of every member (whether solely or jointly
with others) for their debts liabilities and engagements solely or jointly
with any other person to or with the Company whether the time for the
payment fulfilment or discharge thereof shall have actually arrived or
not.
