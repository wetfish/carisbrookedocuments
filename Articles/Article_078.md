78. A Director (with the approval of their co-Directors) may from time to
time appoint in writing under their hand any other person as their proxy to
vote for them at any one or more of the Board Meetings; and such
authority may be general or may be limited to any one or more meetings
or to any specific question or matter and must if required be produced
at any meeting at which the holder proposes to vote.
