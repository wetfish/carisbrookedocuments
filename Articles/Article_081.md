81. The profits of the Company subject to any special rights relating
thereto created or authorised to be created by the regulations and
subject to the provisions of the regulations as to the reserve fund
shall be divisible among the members in proportion to the amount of
capital paid up on the shares held by them respectively at the date of
the declaration of the dividend.
