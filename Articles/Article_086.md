86. The Directors may from time to time pay to the members such interim
dividends as in their judgment the position of the Company justifies.
