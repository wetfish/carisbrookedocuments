44. A resolution in writing signed by all members entitled to vote at
any general Meeting shall be as valid and effectual as if it had been
passed at a meeting of members duly called and constituted. Members
entitled to vote may sign separate copies of the resolution circulated
for that purpose.
