14. If any call or any instalment of any call payable in respect of any
share is not paid by the day appointed for payment thereof the holder
for the time being of such share shall be liable to pay interest on
the same at the rate of ten percent (10%) interest per annum from the
day appointed for the payment thereof up to the day of actual payment
whether such share is forfeited before the day of such actual payment or
not, but the Directors shall be at liberty to waive payment of that
interest wholly or in part.
