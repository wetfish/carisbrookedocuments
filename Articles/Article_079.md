79. The attorney of a member appointed in writing under the hand and
seal of the member and attested by one witness or if the member be a
corporation under its common seal may whether themselves a member of the
Company or not attend and act and vote at all general meetings on behalf
of such member and as their or its proxy without any special appointment
other than their power of attorney or may whether themselves a member of the
Company or not appoint in writing as proxy on behalf of such member a
member of the Company who shall be deemed to be a proxy of such member
for all purposes within the regulations. Any such attorney whether or
not a member of the Company may on behalf of their constituent sign any
resolution consent or instrument which the constituent may under the
Memorandum of Association or Articles be required or entitled to sign.
The attorney shall at least twenty four hours before they shall be
entitled to act or appoint a proxy on behalf of such member produce their
power of attorney or an office copy thereof at the office of the Company
to the secretary who shall record the same and such power of attorney
when so recorded shall be deemed to remain in full force until notice of
revocation thereof shall have been received at the said office.
