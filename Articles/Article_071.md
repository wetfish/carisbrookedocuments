71. Every question at meetings of the Directors or of any committee of
Directors shall be determined by a majority of the votes of the
Directors present every Director having one \(1) vote and in the case of
an equality of votes the acting Chairperson at such meetings shall have a
second or casting vote.
