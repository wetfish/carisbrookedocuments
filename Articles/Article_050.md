50. The instrument appointing any proxy shall be in writing under the
hand of the appointor or if such appointor be a corporation under the
common seal of the corporation.
