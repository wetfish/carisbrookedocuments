96. The Directors shall cause true accounts to be kept of the sums of
money received and expended by the Company and the matters in respect of
which such receipt and expenditure takes place of all sales and
purchases of goods and of the assets and liabilities of the Company. The
books of account shall be kept at the registered office of the Company
or at such other place or places as the Directors think fit and shall at
all times be open to inspection by the Directors.
