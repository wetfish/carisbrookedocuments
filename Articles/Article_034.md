34. The Company may from time to time by special resolution reduce the
capital of the Company in any manner for the time being authorised by
law and the Company may also by special resolution subdivide or
consolidate its shares or any of them or cancel shares which have not
been taken up or agreed to be taken up by any person and the Directors
may subject to the provisions of the Companies Act accept surrenders of
shares.
