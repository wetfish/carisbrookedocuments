38. The business of an Ordinary Meeting shall be to receive
and consider the profit and loss account, the balance sheet and the
reports of the Directors and of the Auditors, to elect Directors
and other officers in the place of those retiring, to declare dividends
and to transact any other business which under the Regulations ought to
be transacted at an Ordinary Meeting and any business which is brought
under consideration by the report of the Directors issued with the
notice convening such meeting. All other business transacted at an
Ordinary Meeting and all business transacted at an Extraordinary Meeting
shall be deemed special.
