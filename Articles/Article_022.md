22. Upon any sale after forfeiture or in purported exercise of the
powers hereinafter contained for enforcing a lien the Directors may cause
the purchaser's name to be entered in the Register in respect of the
shares sold and the purchaser shall not be bound to see to the
regularity of the proceedings or to the application of the purchase
money; and after their name has been entered in the Register the
validity of the sale shall not be impeached by any person and the
remedy of any person aggrieved by the sale shall be in damages only
and against the Company exclusively.
