72. A meeting of the Directors at which a quorum is present shall be
competent to exercise all or any of the authorities powers and
discretions by or under the regulations of the Company for the time
being vested in or exercisable by the Directors generally.
