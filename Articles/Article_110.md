110. If the Company shall be wound up and the assets available for
distribution among the members as such without calling up or treating as
called up any uncalled capital shall be insufficient to repay the whole
of the paid-up capital such assets shall be distributed among the
members in proportion to the amounts actually paid up or deemed to be
paid up or which in obedience to any calls made before the commencement
of the winding up ought to have been paid up at the commencement of the
winding up on the shares held by them respectively. And if in a winding
up the assets available for distribution among the members without
calling up or treating as called up any uncalled capital shall be more
than sufficient to repay the whole of the capital paid up at the
commencement of the winding up the excess shall be distributed amongst
the members in proportion to the capital at the commencement of the
winding up paid up or deemed to be paid up or which ought to have been
paid up on the shares held by them respectively. But this clause is to
be without prejudice to the rights of the holders of shares issued upon
special terms and conditions.
