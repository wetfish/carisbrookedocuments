104. All notices given to shareholders shall with respect to any shares
to which persons are jointly entitled be given to whichever of such
persons is named first in the register of shareholders and notice so
given shall be sufficient notice to all holders of such shares.
