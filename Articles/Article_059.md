59. Unless it be resolved to reduce the number of Directors the Ordinary
General Meeting at which Directors retire shall elect a successor to
each retiring Director. A retiring Director shall remain in office until
the close of the meeting and notwithstanding the election of their
successor.
