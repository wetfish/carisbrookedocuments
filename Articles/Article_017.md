17. If the requisitions of any such notice shall not be complied with any share
    in respect of which such notice shall have been given may without any
    further notice at any time thereafter and before payment of all calls levies
    interest and expenses due in respect thereof has been made be forfeited by a
    resolution of the Directors to that effect.
    
    a. Where any shares shall have been so forfeited, written notice of the
    resolution shall be given to the member in whose name the said shares stood
    immediately prior to the forfeiture and an entry of the forfeiture with the
    date thereof shall forthwith be made in the register.
