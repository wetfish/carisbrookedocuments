6.  The shares of the Company shall be held by the members in groups of shares
    according to the respective flats as set out hereunder. The holder of a
    group of shares shall, subject to the conditions herein and in Article 6(a)
    contained, have the full right to occupation of the flat in respect of which
    such group of shares is held and such holder shall not be liable for payment
    of rent or any other payment whatsoever except the levy or charges provided
    under these Articles in respect of their occupation of such flat. Subject to
    the approval of the Board first had and obtained the holder of each such
    group of shares shall have the right of sub-leasing the flat in respect of
    which such group of shares is held. The flats available for the holders of
    the said groups of shares shall be as follows:-

    \hfill \break
    Flat 1 for the holder of 1100 shares No. 1 to 1100 incl.

    Flat 2 for the holder of 1100 shares No. 1101 to 2200 incl.

    Flat 3 for the holder of 1700 shares No. 2201 to 3900 incl.

    Flat 4 for the holder of 1700 shares No. 3901 to 5600 incl.

    Flat 5 for the holder of 2100 shares No. 5601 to 7700 incl.

    Flat 6 for the holder of 2100 shares No. 7701 to 9800 incl.

    Flat 7 for the holder of 2100 shares No. 9801 to 11900 incl.

    Flat 8 for the holder of 2150 shares No. 11901 to 14050 incl.

    Flat 9 for the holder of 2200 shares No. 14051 to 16250 incl.

    Flat 10 for the holder of 2200 shares No. 16251 to 18450 incl.

    Flat 11 for the holder of 2200 shares No. 18451 to 20650 incl.

    Flat 12 for the holder of 2250 shares No. 20651 to 22900 incl.

    Flat 14 for the holder of 2300 shares No. 22901 to 25200 incl.

    Flat 15 for the holder of 2300 shares No. 25201 to 27500 incl.

    Flat 16 for the holder of 2300 shares No. 27501 to 29800 incl.

    Flat 17 for the holder of 2350 shares No. 29801 to 32150 incl.

    Flat 18 for the holder of 2400 shares No. 32151 to 34550 incl.

    Flat 19 for the holder of 2400 shares No. 34551 to 36950 incl.

    Flat 20 for the holder of 2600 shares No. 36951 to 39550 incl.

    Flat 21 for the holder of 2700 shares No. 39551 to 42250 incl.

    Flat 22 for the holder of 2850 shares No. 42251 to 45100 incl.

    Flat 23 for the holder of 2900 shares No. 45101 to 48000 incl.

    Flat 24 for the holder of 2700 shares No. 48001 to 50700 incl.

    Flat 25 for the holder of 2800 shares No. 50701 to 53500 incl.

    Flat 26 for the holder of 2950 shares No. 53501 to 56450 incl.

    Flat 27 for the holder of 3000 shares No. 56451 to 59450 incl.

    Penthouse for the holder of 6100 shares No. 59451 to 65550 incl.

    Flat 1A for the holder of 550 shares No. 65551 to 66100 incl.

    \hfill \break
    The holder of each such group of shares shall subject to the conditions
    herein and in Article 6(a) contained have the full use of the grounds,
    entrance hall, passage ways, laundries and general conveniences of the
    building and shall not be liable for payment of rent or any other payment
    whatsoever except the levy or charges provided under these Articles in
    respect of such uses.

    \hfill \break
    6(a). In respect of any resolution that may at any time be proposed for
    amendment or deletion or having the effect of amending or deleting Article
    6, 6(a) or 6(b), any dissenting shareholder (irrespective of the number of
    groups of shares held) shall be entitled to and shall have a number of votes
    equal to twentysix percent (26%) of the total votes possible on such
    resolution notwithstanding the provisions of Articles 45 and 48.
    
    \hfill \break
    6(b). The rights conferred by this Article upon a member shall be subject to
    the following conditions:-

    a. A member shall ensure that at their own expense the interior of such flat
    and the fixtures therein are kept clean and in good order and condition and
    are from time to time properly painted.

    b. The Company its servants agents and employees shall have the right at all
    reasonable times to enter and remain in such flat for all reasonable
    purposes in connection with inspection of the state of cleanliness, order,
    condition and painting and in connection with the inspection, installation,
    repair, maintenance or alteration of gas, water and electrical fittings,
    pipes, wires or other apparatus serving or affecting the service of gas,
    water or electricity to other parts of the building and in connection with
    any other necessary purposes.

    c. If any merchandise or property that may be about the said buildings shall
    be injured or destroyed by water or otherwise, no part of such loss or
    damage shall be borne by the Company unless the same shall occur by reason
    of the carelessness or negligence of any servant or agent of the Company.

    d. A member shall not do or permit or suffer anything to be done in or about
    the building or bring or keep anything therein which will in any way
    obstruct or interfere with the rights of other occupants of the buildings or
    in any way interfere with or annoy them or which will increase the rate of
    fire insurance on the said buildings or on property kept therein or conflict
    with the laws relating to fires or with the by-laws or regulations of the
    Board of Fire Commissioners of New South Wales or such like authority or
    with any insurance policy upon the said buildings or any part thereof or
    conflict with any of the laws, by-laws, rules or ordinances in force of the
    Municipal Council or of the Metropolitan Water Sewerage and Drainage Board
    or the Board of Health.

    e. The member shall not throw or allow to fall or permit or suffer to be
    thrown or to fall any paper, rubbish, refuse or other substance whatsoever
    out of the windows or doors or down the staircases, passages or skylights of
    the building. Any damage or cost for cleaning or repair caused by breach
    hereof shall be borne by the member.

    f. No animals shall be kept in or about the building except at the pleasure
    of the Board.

    g. The member shall only use or permit or suffer the flat to be used as a
    private dwelling house and for no other purpose whatsoever.

    h. The member shall not permit or suffer any person of unsound mind or a
    drunkard or a person of immoral life to reside in or be upon the said flat.

    i. The member will ensure that the flat is not used in any manner so as to
    constitute a detriment, nuisance, annoyance or inconvenience to neighbouring
    owners or occupiers including the occupants of other flats in the building
    or to be used for any illegal or immoral purpose.

    j. The member will not deposit anything or throw any dust or beat any mat,
    carpet or cloth on or in or allow children to obstruct the use by
    other occupants of the building, or persons reasonably using the building,
    of the entrance hall, passages, stairways or lifts.

    k. No clothing, bedding or other articles shall be hung on the windows,
    balconies or on the outside of the building or windows thereof. In the event
    of the Directors resolving that, in their opinion, a member has failed to
    observe any of the conditions set out above or in Article 6, they may call
    upon such member to remedy the breach and if such breach be not remedied to
    their satisfaction within such time or upon such other conditions as they
    shall notify to the member, then the Directors shall call a special general
    meeting of the Company to consider whether the member in question shall be
    required to cease to be a member of the Company. If such meeting by special
    resolution resolves that they shall cease to be a member, then the following
    provisions shall apply:-

        i. The member in question may within two months from the date of such
        meeting or such further time as such meeting may allow, but subject to
        these Articles, transfer their shares to any person approved by the
        Directors.

        ii. If they shall not within such time have found a transferee approved by
        the Directors and have transferred said shares to such person then the
        Company may find a purchaser for such shares and sell to such purchaser
        such shares at such price as the Directors may determine and sign all
        necessary documents for the purpose of transferring the shares of the
        member aforesaid to such purchaser and may receive on behalf of such
        member the purchase price and if the member aforesaid, having been
        called upon to do so, does not surrender to such transferee or to the
        Company the certificates for such shares then such certificates may be
        cancelled by the Company and new certificates issued therefor and the
        Company is by these Articles irrevocably authorised by every member to
        do all such acts and is hereby constituted for such purposes the
        Attorney of every member from time to time of the Company.
