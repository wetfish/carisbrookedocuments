90. A transfer of shares shall not pass the right to any dividend
declared thereon after such transfer and before the registration of the
transfer.
