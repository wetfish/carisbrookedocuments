20. Until any share so forfeited shall be sold re-allotted or otherwise
disposed of the forfeiture thereof may at the discretion and by a
resolution of the Directors be remitted on such terms as the Directors
may in their discretion think fit.
