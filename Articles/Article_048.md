48. Upon a poll every member present in person or by proxy or attorney
shall have one \(1) vote for every share in the Company held by them.
