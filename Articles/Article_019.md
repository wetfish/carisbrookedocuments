19. Any share forfeited under these Articles shall be deemed to be the property
    of the Company and may be sold or re-allotted or otherwise disposed of for
    the benefit of the Company in such manner as the Directors shall approve.
    Provided that in the event of any shares so forfeited being sold within
    twelve \(12) months from the date of forfeiture, any residue remaining from
    the proceeds thereof after satisfaction of any calls levies or instalments
    due and unpaid in respect of such share and accrued interest and expenses
    shall be paid to the person entitled to such share at the time of forfeiture
    their executors administrators or assigns.
