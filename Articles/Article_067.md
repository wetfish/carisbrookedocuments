67. A quorum at meetings of the Directors shall be such number as may be
    determined by the Directors but unless so determined four \(4) shall form a
    quorum. A Director interested is to be counted in a quorum notwithstanding
    their interest.
