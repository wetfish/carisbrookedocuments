92. The Directors may deduct from the dividends or interest payable to
any member all such sums of money as may be due from them to the Company
on account of calls or otherwise.
