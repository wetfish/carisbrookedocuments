97. The Directors shall, in accordance with Section 1300 of the Corporations
Act, ensure that any book that must be maintained by the Company under the
Corporations Act be available for inspection within 7 days of a written request
from a Member.
