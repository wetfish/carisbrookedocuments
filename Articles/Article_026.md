26. Subject to the provisions hereinafter contained shares may be
transferred in the usual common form or in such other form signed by the
transferor and transferee as the Directors may think fit to accept. The
transferor shall be deemed to remain the holder of any share
transferred until the name of the transferee is entered on the register
as the holder thereof.
