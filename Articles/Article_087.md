87. Any general meeting declaring a dividend may resolve that such
dividend be paid wholly or in part by the distribution of specific
assets and in particular of paid up shares debentures or debenture stock
of the Company or paid up shares debentures or debenture stock of any
other company or in any one or more of such ways.
